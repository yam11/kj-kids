// ignore_for_file: public_member_api_docs, sort_constructors_first
part of 'teacher_bloc.dart';

@immutable
abstract class TeacherEvent {}

class TeacherList extends TeacherEvent {}

class GetTeacherProFile extends TeacherEvent {
  final int teacherId;
  GetTeacherProFile({
    required this.teacherId,
  });
}
