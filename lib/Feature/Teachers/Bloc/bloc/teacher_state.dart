part of 'teacher_bloc.dart';

@immutable
abstract class TeacherState {}

class TeacherInitial extends TeacherState {}

class ErrorToGetListOfTeacher extends TeacherState {
  final String message;
  ErrorToGetListOfTeacher({required this.message});
}

class SuccesToGetListOfTeacher extends TeacherState {}

class LoadingToGetListOfTeacher extends TeacherState {}

class ErrorToGetTeacherProfile extends TeacherState {
  final String message;
  ErrorToGetTeacherProfile({required this.message});
}

class SuccesToGetTeacherProfile extends TeacherState {}

class LoadingToGetTeacherProfile extends TeacherState {}