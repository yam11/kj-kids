import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:junior/Core/Api/ExceptionsHandle.dart';
import 'package:junior/Core/Api/Network.dart';
import 'package:junior/Core/Api/Urls.dart';
import 'package:junior/Feature/Teachers/Models/certificate_model.dart';
import 'package:junior/Feature/Teachers/Models/teacher_list_model.dart';
import 'package:junior/Feature/Teachers/Models/teacher_model.dart';
import 'package:meta/meta.dart';

part 'teacher_event.dart';
part 'teacher_state.dart';

class TeacherBloc extends Bloc<TeacherEvent, TeacherState> {
  ListOfTeacherModel? listOfTeacherModel;
  TeacherModel? teacherModel;
  CertificateModel? certificateModel;
  TeacherBloc() : super(TeacherInitial()) {
    on<TeacherEvent>((event, emit) async {
      if (event is TeacherList) {
        emit(LoadingToGetListOfTeacher());
        try {
         await Future.delayed(const  Duration(seconds: 2));
          final response = await Network.getData(url: Urls.teachersList);

          listOfTeacherModel = ListOfTeacherModel.fromJson(response.data);

          emit(SuccesToGetListOfTeacher());
        } catch (error) {
          if (error is DioError) {
            emit(ErrorToGetListOfTeacher(
                message: exceptionsHandle(error: error)));
          } else {
            emit(ErrorToGetListOfTeacher(message: "error"));
          }
        }
      }
      if (event is GetTeacherProFile) {
        emit(LoadingToGetTeacherProfile());
        try {
          final response1 = await Network.getData(
              url: "${Urls.teachersProfile}${event.teacherId}");
          final response2 = await Network.getData(
              url: "${Urls.teacherCertificates}${event.teacherId}");

          teacherModel = TeacherModel.fromJson(response1.data);
          certificateModel = CertificateModel.fromJson(response2.data);
          emit(SuccesToGetTeacherProfile());
        } catch (error) {
          if (error is DioError) {
            emit(ErrorToGetTeacherProfile(
                message: exceptionsHandle(error: error)));
          } else {
            emit(ErrorToGetTeacherProfile(message: "error"));
          }
        }
      }
    });
  }
}
