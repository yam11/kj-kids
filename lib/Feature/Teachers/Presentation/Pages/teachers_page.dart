import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:junior/App/app_localizations.dart';
import 'package:junior/Core/Constants/app_assets.dart';
import 'package:junior/Core/Constants/app_colors.dart';
import 'package:junior/Core/Widgets/app_bar_widget.dart';
import 'package:junior/Core/Widgets/back_graound_widget.dart';
import 'package:junior/Core/Widgets/error_message_widget.dart';
import 'package:junior/Core/Widgets/loading_widget.dart';
import 'package:junior/Core/Widgets/tilte_page_widget.dart';
import 'package:junior/Feature/Home/Presentation/Widgets/teacher_card_widget.dart';
import 'package:junior/Feature/Teachers/Bloc/bloc/teacher_bloc.dart';
import 'package:junior/Feature/Teachers/Presentation/Pages/teacher_profile_page.dart';
import 'package:loading_indicator/loading_indicator.dart';
import 'package:page_transition/page_transition.dart';
import 'package:shimmer/shimmer.dart';
import 'package:sizer/sizer.dart';

class TeacherPage extends StatelessWidget {
  TeacherPage({Key? key}) : super(key: key);
  TeacherBloc teacherBloc = TeacherBloc();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        extendBody: true,
        extendBodyBehindAppBar: true,
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(AppBar().preferredSize.height),
          child: const AppBarWidget(),
        ),
        body: BackGraoundWidget(
            child: SafeArea(
                child: Column(
          children: [
            FadeInDown(
              duration: const Duration(milliseconds: 1600),
              child: TitlePageWidget(
                title: "All Teacher".tr(context),
              ),
            ),
            Expanded(
              child: BlocProvider(
                create: (context) => teacherBloc..add(TeacherList()),
                child: BlocConsumer<TeacherBloc, TeacherState>(
                  listener: (context, state) {},
                  builder: (context, state) {
                    if (state is ErrorToGetListOfTeacher) {
                      return ErrorMessageWidget(
                        onPressed: () {
                          teacherBloc.add(TeacherList());
                        },
                        message: state.message,
                      );
                    }
                    if (state is SuccesToGetListOfTeacher) {
                      return GridView.builder(
                        itemCount: teacherBloc.listOfTeacherModel!.data!.length,
                        gridDelegate:
                            const SliverGridDelegateWithFixedCrossAxisCount(
                                crossAxisCount: 2,
                                crossAxisSpacing: 5,
                                childAspectRatio: 0.8,
                                mainAxisSpacing: 10),
                        physics: const BouncingScrollPhysics(),
                        itemBuilder: (context, index) {
                          return TeacherCardWidget(
                            teacherName: teacherBloc.listOfTeacherModel!
                                    .data![index].firstName! +
                                teacherBloc
                                    .listOfTeacherModel!.data![index].lastName!,
                            teacherImage: AppAssets.womanIcon,
                            onPressed: () {
                              Navigator.push(
                                  context,
                                  PageTransition(
                                      child: TeacherProFile(
                                          teacherId: teacherBloc
                                              .listOfTeacherModel!
                                              .data![index]
                                              .id!),
                                      type: PageTransitionType.rightToLeft,
                                      duration:
                                          const Duration(milliseconds: 300)));
                            },
                          );
                        },
                      );
                    } else {
                      return AnimationLimiter(
                        child: Shimmer.fromColors(
                            baseColor:
                                AppColors.seconedaryColor.withOpacity(0.5),
                            highlightColor: Colors.grey[100]!,
                            enabled: true,
                            child: GridView.count(
                              crossAxisCount: 2,
                              physics: const BouncingScrollPhysics(
                                  parent: AlwaysScrollableScrollPhysics()),
                              padding: EdgeInsets.symmetric(vertical: 2.h),
                              children: List.generate(
                                  50,
                                  (index) =>
                                      AnimationConfiguration.staggeredGrid(
                                          position: index,
                                          columnCount: 2,
                                          child: ScaleAnimation(
                                              duration: const Duration(
                                                  milliseconds: 900),
                                              curve:
                                                  Curves.fastLinearToSlowEaseIn,
                                              child: Container(
                                                margin: EdgeInsets.symmetric(
                                                    vertical: 4.h),
                                                color: Colors.transparent,
                                                child: Column(children: [
                                                  Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .center,
                                                    children: [
                                                      CircleAvatar(
                                                        radius: 8.w,
                                                        backgroundColor:
                                                            AppColors
                                                                .primaryColor,
                                                        backgroundImage:
                                                            const AssetImage(
                                                                AppAssets
                                                                    .teacherIcon),
                                                      ),
                                                    ],
                                                  ),
                                                  SizedBox(
                                                    height: 1.h,
                                                  ),
                                                  Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .center,
                                                    children: [
                                                      Text(
                                                        "Teacher Name",
                                                        style: TextStyle(
                                                            color: AppColors
                                                                .seconedaryColor,
                                                            fontSize: 14.sp,
                                                            fontWeight:
                                                                FontWeight
                                                                    .bold),
                                                      ),
                                                    ],
                                                  ),
                                                  SizedBox(
                                                    height: 1.h,
                                                  ),
                                            
                                                ]),
                                              )))),
                            )),
                      );
                    }
                  },
                ),
              ),
            ),
          ],
        ))));
  }
}
