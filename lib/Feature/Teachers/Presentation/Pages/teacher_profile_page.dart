import 'package:animate_do/animate_do.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:junior/App/app_localizations.dart';
import 'package:junior/Core/Constants/app_assets.dart';
import 'package:junior/Core/Constants/app_colors.dart';
import 'package:junior/Core/Widgets/app_bar_widget.dart';
import 'package:junior/Core/Widgets/back_graound_widget.dart';
import 'package:junior/Core/Widgets/error_message_widget.dart';
import 'package:junior/Core/Widgets/loading_widget.dart';
import 'package:junior/Core/Widgets/tilte_page_widget.dart';
import 'package:junior/Core/Widgets/title_widget.dart';
import 'package:junior/Feature/Comments/Models/comments_model.dart';
import 'package:junior/Feature/Comments/Presentation/Pages/commenst_page.dart';
import 'package:junior/Feature/Comments/Presentation/Widget/comments_widget.dart';
import 'package:junior/Feature/Comments/Presentation/Widget/write_comment_widget.dart';
import 'package:junior/Core/Widgets/carousel_slider_image.dart';
import 'package:junior/Feature/Teachers/Bloc/bloc/teacher_bloc.dart';
import 'package:junior/Feature/Teachers/Presentation/Widgets/teacher_profile_widget.dart';
import 'package:page_transition/page_transition.dart';
import 'package:sizer/sizer.dart';

class TeacherProFile extends StatefulWidget {
  int teacherId;
  TeacherProFile({required this.teacherId, Key? key}) : super(key: key);

  @override
  State<TeacherProFile> createState() => _TeacherProFileState();
}

class _TeacherProFileState extends State<TeacherProFile> {
  final TextEditingController commentsController = TextEditingController();

  final controller = CarouselController();
  int activeIndex = 0;
  TeacherBloc teacherBloc = TeacherBloc();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBody: true,
      extendBodyBehindAppBar: true,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(AppBar().preferredSize.height),
        child: const AppBarWidget(),
      ),
      body: BackGraoundWidget(
          child: SafeArea(
        child: Column(
          children: [
            FadeInDown(
                duration: const Duration(milliseconds: 1600),
                child: TitlePageWidget(title: "Teacher Profile".tr(context))),
            Expanded(
              child: BlocProvider(
                create: (context) => teacherBloc
                  ..add(GetTeacherProFile(teacherId: widget.teacherId)),
                child: BlocConsumer<TeacherBloc, TeacherState>(
                  listener: (context, state) {
                    // TODO: implement listener
                  },
                  builder: (context, state) {
                    if (state is ErrorToGetTeacherProfile) {
                      return ErrorMessageWidget(
                          onPressed: () {
                            teacherBloc.add(
                                GetTeacherProFile(teacherId: widget.teacherId));
                          },
                          message: state.message);
                    }
                    if (state is SuccesToGetTeacherProfile) {
                      return ListView(
                        physics: const BouncingScrollPhysics(),
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              CircleAvatar(
                                radius: 10.w,
                                backgroundColor: AppColors.primaryColor,
                                backgroundImage:
                                    const AssetImage(AppAssets.womanIcon),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 3.h,
                          ),
                          TeacherProfileWidget(
                              teacherModel: teacherBloc.teacherModel!),
                          SizedBox(
                            height: 3.h,
                          ),
                          TitleWidget(
                              title: "Scientific certificates obtained"
                                  .tr(context)),
                          SizedBox(
                            height: 2.h,
                          ),
                          Container(
                            height: 30.h,
                            margin: EdgeInsets.symmetric(horizontal: 5.w),
                            decoration: BoxDecoration(
                                color: AppColors.whiteColor,
                                border: Border.all(
                                    color: AppColors.grayColor, width: 2),
                                borderRadius: BorderRadius.circular(5.w),
                                image: DecorationImage(
                                    image: NetworkImage(teacherBloc
                                        .certificateModel!.data!.certificate!),
                                    fit: BoxFit.fill)),
                          ),
                          SizedBox(
                            height: 2.h,
                          ),
                          TitleWidget(
                              title: "Share your opinion about teacher"
                                  .tr(context)),
                          SizedBox(
                            height: 2.h,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              TextButton(
                                  onPressed: () {
                                    Navigator.of(context).push(
                                      PageTransition(
                                          child: CommentsPage(commendModel: commendForNanny),
                                          type: PageTransitionType.rightToLeft,
                                          duration: const Duration(
                                              milliseconds: 300)),
                                    );
                                  },
                                  child: Row(
                                    children: [
                                      Text(
                                        "All Opinion".tr(context),
                                        style: TextStyle(
                                            color: AppColors.seconedaryColor,
                                            fontSize: 14.sp,
                                            fontWeight: FontWeight.bold),
                                      ),
                                      Icon(
                                        Icons.arrow_forward,
                                        color: AppColors.seconedaryColor,
                                        size: 14.sp,
                                      )
                                    ],
                                  )),
                            ],
                          ),
                          for (int i = 0; i < 2; i++)
                            CommentsWidget(
                              avatarImage: AppAssets.womanIcon,
                              commendModel: commendForNanny,
                           index: i,
                            ),
                          WriteCommentsWidget(
                            type: "nanny",
                          ),
                          SizedBox(
                            height: 5.h,
                          )
                        ],
                      );
                    } else {
                      return const LoadingWidget();
                    }
                  },
                ),
              ),
            )
          ],
        ),
      )),
    );
  }
}
