import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:junior/App/app_localizations.dart';
import 'package:junior/Core/Constants/app_colors.dart';
import 'package:junior/Core/Widgets/comments_form_falid_widget.dart';
import 'package:junior/Feature/Comments/Bloc/bloc/comments_bloc.dart';

import 'package:sizer/sizer.dart';

class WriteCommentsWidget extends StatelessWidget {
  String type;
  WriteCommentsWidget({required this.type, Key? key}) : super(key: key);
  CommentsBloc commentsBloc = CommentsBloc();
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => commentsBloc,
      child: Column(
        children: [
          CommentsFormFalidWidget(
            maxLines: 4,
            textInputType: TextInputType.text,
            controller: commentsBloc.messageController,
          ),
          SizedBox(
            height: 2.h,
          ),
          BlocConsumer<CommentsBloc, CommentsState>(
            listener: (context, state) {
              if (state is SuccesToAddComments) {
                commentsBloc.messageController.clear();
              }
            },
            builder: (context, state) {
              return Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  state is! LoadingToAddComments
                      ? MaterialButton(
                          onPressed: () {
                            if (type == "food") {
                              commentsBloc.add(AddCommentsFood());
                            } else if (type == "nanny") {
                              commentsBloc.add(AddCommentsNanny());
                            } else {
                              commentsBloc.add(AddCommentsinGeneral());
                            }
                          },
                          padding: EdgeInsets.symmetric(
                              horizontal: 3.w, vertical: 2.h),
                          color: AppColors.primaryColor,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(5.w)),
                          child: Text(
                            "Send".tr(context),
                            style: TextStyle(
                                color: AppColors.whiteColor,
                                fontSize: 15.sp,
                                fontWeight: FontWeight.bold),
                          ),
                        )
                      : const CircularProgressIndicator(
                          color: AppColors.primaryColor,
                        ),
                ],
              );
            },
          ),
        ],
      ),
    );
  }
}
