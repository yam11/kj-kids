import 'package:flutter/material.dart';
import 'package:junior/Core/Constants/app_assets.dart';
import 'package:junior/Core/Widgets/app_bar_widget.dart';
import 'package:junior/Core/Widgets/back_graound_widget.dart';
import 'package:junior/Feature/Comments/Models/comments_model.dart';
import 'package:junior/Feature/Comments/Presentation/Widget/comments_widget.dart';

class CommentsPage extends StatelessWidget {
  List<CommentsModel> commendModel;
  CommentsPage({required this.commendModel, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        extendBody: true,
        extendBodyBehindAppBar: true,
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(AppBar().preferredSize.height),
          child: const AppBarWidget(),
        ),
        body: BackGraoundWidget(
            child: SafeArea(
                child: ListView.builder(
          physics: const BouncingScrollPhysics(),
          itemCount: commendModel.length,
          itemBuilder: (context, index) {
            return CommentsWidget(
              avatarImage: AppAssets.womanIcon,
              commendModel: commendModel,
              index: index,
            );
          },
        ))));
  }
}
