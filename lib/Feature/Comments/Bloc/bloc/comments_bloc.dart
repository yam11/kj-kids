import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:junior/Feature/Comments/Models/comments_model.dart';
import 'package:meta/meta.dart';

part 'comments_event.dart';
part 'comments_state.dart';

class CommentsBloc extends Bloc<CommentsEvent, CommentsState> {
  List<CommentsModel> commentsModel = [];
  TextEditingController messageController = TextEditingController();
  CommentsBloc() : super(CommentsInitial()) {
    on<CommentsEvent>((event, emit) async {
      if (event is AddCommentsinGeneral) {
        emit(LoadingToAddComments());
        await Future.delayed(const Duration(seconds: 1));
        commend.add(
            CommentsModel(userName: "Pearl", commend: messageController.text));
        emit(SuccesToAddComments());
      }
      if (event is AddCommentsFood) {
        emit(LoadingToAddComments());
        await Future.delayed(const Duration(seconds: 1));
        commendForFood.add(
            CommentsModel(userName: "Pearl", commend: messageController.text));
        emit(SuccesToAddComments());
      }
      if (event is AddCommentsNanny) {
        emit(LoadingToAddComments());
        await Future.delayed(const Duration(seconds: 1));
        commendForNanny.add(
            CommentsModel(userName: "Pearl", commend: messageController.text));
        emit(SuccesToAddComments());
      }
    });
  }
}
