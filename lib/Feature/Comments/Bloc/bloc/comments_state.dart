part of 'comments_bloc.dart';

@immutable
abstract class CommentsState {}

class CommentsInitial extends CommentsState {}
class LoadingToAddComments extends CommentsState {}

class SuccesToAddComments extends CommentsState {}
