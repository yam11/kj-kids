part of 'comments_bloc.dart';

@immutable
abstract class CommentsEvent {}
class AddCommentsinGeneral extends CommentsEvent{}
class AddCommentsFood extends CommentsEvent{}
class AddCommentsNanny extends CommentsEvent{}
