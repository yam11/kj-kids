class ProfileModel {
  Data? data;

  ProfileModel({this.data});

  ProfileModel.fromJson(Map<String, dynamic> json) {
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data!.toJson();
    }
    return data;
  }
}

class Data {
  int? id;
  String? firstName;
  String? lastName;
  String? address;
  String? phoneNumber;
  int? balance;
  List<Children>? children;
  List<Orders>? orders;

  Data(
      {this.id,
      this.firstName,
      this.lastName,
      this.address,
      this.phoneNumber,
      this.balance,
      this.children,
      this.orders});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    firstName = json['firstName'];
    lastName = json['LastName'];
    address = json['address'];
    phoneNumber = json['phoneNumber'];
    balance = json['balance'];
    if (json['children'] != null) {
      children = <Children>[];
      json['children'].forEach((v) {
        children!.add(new Children.fromJson(v));
      });
    }
    if (json['orders'] != null) {
      orders = <Orders>[];
      json['orders'].forEach((v) {
        orders!.add(new Orders.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['firstName'] = this.firstName;
    data['LastName'] = this.lastName;
    data['address'] = this.address;
    data['phoneNumber'] = this.phoneNumber;
    data['balance'] = this.balance;
    if (this.children != null) {
      data['children'] = this.children!.map((v) => v.toJson()).toList();
    }
    if (this.orders != null) {
      data['orders'] = this.orders!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Children {
  int? id;
  String? firstName;
  int? guaridanId;
  int? classroomId;
  String? status;
  String? birthDate;

  Children(
      {this.id,
      this.firstName,
      this.guaridanId,
      this.classroomId,
      this.status,
      this.birthDate});

  Children.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    firstName = json['firstName'];
    guaridanId = json['guaridanId'];
    classroomId = json['classroomId'];
    status = json['status'];
    birthDate = json['birthDate'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['firstName'] = this.firstName;
    data['guaridanId'] = this.guaridanId;
    data['classroomId'] = this.classroomId;
    data['status'] = this.status;
    data['birthDate'] = this.birthDate;
    return data;
  }
}

class Orders {
  int? id;
  int? childId;
  int? totalPrice;

  Orders({this.id, this.childId, this.totalPrice});

  Orders.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    childId = json['childId'];
    totalPrice = json['totalPrice'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['childId'] = this.childId;
    data['totalPrice'] = this.totalPrice;
    return data;
  }
}
