part of 'profile_bloc.dart';

@immutable
abstract class ProfileState {}

class ProfileInitial extends ProfileState {}

class SuccesToGetProfile extends ProfileState {}

class ErrorToGetProfile extends ProfileState {
  final String message;
  ErrorToGetProfile({required this.message});
}

class LoadingToGetProfile extends ProfileState {}
