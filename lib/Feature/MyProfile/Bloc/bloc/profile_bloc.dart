import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:junior/Core/Api/ExceptionsHandle.dart';
import 'package:junior/Core/Api/Network.dart';
import 'package:junior/Core/Api/Urls.dart';
import 'package:junior/Core/Util/SharedPreferences/SharedPreferencesHelper.dart';
import 'package:junior/Feature/MyProfile/models/profile_model.dart';
import 'package:meta/meta.dart';

part 'profile_event.dart';
part 'profile_state.dart';

class ProfileBloc extends Bloc<ProfileEvent, ProfileState> {
  ProfileModel? profileModel;
  ProfileBloc() : super(ProfileInitial()) {
    on<ProfileEvent>((event, emit) async {
      if (event is GetProfile) {
        emit(LoadingToGetProfile());
        try {
          final response = await Network.getData(url: "${Urls.guardians}3");
          profileModel = ProfileModel.fromJson(response.data);
          emit(SuccesToGetProfile());
        } catch (error) {
          if (error is DioError) {
            emit(ErrorToGetProfile(message: exceptionsHandle(error: error)));
          } else {
            emit(ErrorToGetProfile(message: "error"));
          }
        }
      }
    });
  }
}
