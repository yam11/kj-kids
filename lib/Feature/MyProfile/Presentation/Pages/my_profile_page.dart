import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:junior/App/app_localizations.dart';
import 'package:junior/Core/Constants/app_assets.dart';
import 'package:junior/Core/Constants/app_colors.dart';
import 'package:junior/Core/Widgets/app_bar_widget.dart';
import 'package:junior/Core/Widgets/back_graound_widget.dart';
import 'package:junior/Core/Widgets/error_message_widget.dart';
import 'package:junior/Core/Widgets/loading_widget.dart';
import 'package:junior/Core/Widgets/tilte_page_widget.dart';
import 'package:junior/Feature/MyProfile/Bloc/bloc/profile_bloc.dart';
import 'package:junior/Feature/MyProfile/Presentation/Widgets/Profile_widget.dart';
import 'package:junior/Feature/MyProfile/Presentation/Widgets/more_info_widget.dart';
import 'package:sizer/sizer.dart';

class MyProfilePage extends StatefulWidget {
  const MyProfilePage({Key? key}) : super(key: key);

  @override
  State<MyProfilePage> createState() => _MyProfilePageState();
}

class _MyProfilePageState extends State<MyProfilePage> {
  bool animated = false;
  @override
  void initState() {
    startAnimation();
    super.initState();
  }

  Future startAnimation() async {
    await Future.delayed(const Duration(milliseconds: 500));
    setState(() {
      animated = true;
    });
  }

  ProfileBloc profileBloc = ProfileBloc();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBody: true,
      extendBodyBehindAppBar: true,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(AppBar().preferredSize.height),
        child: const AppBarWidget(),
      ),
      body: BackGraoundWidget(
        child: SafeArea(
          child: Column(
            children: [
              FadeInDown(
                  duration: const Duration(milliseconds: 1600),
                  child: TitlePageWidget(title: "My Profile".tr(context))),
              Expanded(
                child: BlocProvider(
                  create: (context) => profileBloc..add(GetProfile()),
                  child: BlocConsumer<ProfileBloc, ProfileState>(
                    listener: (context, state) {},
                    builder: (context, state) {
                      if (state is ErrorToGetProfile) {
                        return ErrorMessageWidget(
                            onPressed: () {
                              profileBloc.add(GetProfile());
                            },
                            message: state.message);
                      }
                      if (state is SuccesToGetProfile) {
                        return SingleChildScrollView(
                            physics: const BouncingScrollPhysics(),
                            child: Column(
                              children: [
                                SizedBox(
                                  height: 4.h,
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    AnimatedOpacity(
                                      duration:
                                          const Duration(milliseconds: 350),
                                      opacity: animated ? 1 : 0,
                                      child: CircleAvatar(
                                        radius: 10.w,
                                        backgroundColor: AppColors.primaryColor,
                                        backgroundImage: const AssetImage(
                                            AppAssets.womanIcon),
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: 3.h,
                                ),
                                AnimatedOpacity(
                                  duration: const Duration(milliseconds: 750),
                                  opacity: animated ? 1 : 0,
                                  child: ProfileWidget(
                                    name: profileBloc
                                        .profileModel!.data!.firstName!,
                                    lastName: profileBloc
                                        .profileModel!.data!.lastName!,
                                    address: profileBloc
                                        .profileModel!.data!.address!,
                                  ),
                                ),
                                SizedBox(
                                  height: 3.h,
                                ),
                                AnimatedOpacity(
                                  duration: const Duration(milliseconds: 1500),
                                  opacity: animated ? 1 : 0,
                                  child: MoreInfoWidget(
                              
                                    phone:    profileBloc
                                            .profileModel!.data!.phoneNumber!,
                                    numberOfChildren: profileBloc
                                        .profileModel!.data!.children!.length,
                                    balance: profileBloc
                                            .profileModel!.data!.balance!,
                                  ),
                                )
                              ],
                            ));
                      } else {
                        return const LoadingWidget();
                      }
                    },
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
