import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:junior/Core/Constants/app_colors.dart';
import 'package:loading_indicator/loading_indicator.dart';

import 'package:map_launcher/map_launcher.dart' as maplauncher;
import 'package:sizer/sizer.dart';

class MapPage extends StatefulWidget {
  MapPage({Key? key}) : super(key: key);

  @override
  State<MapPage> createState() => _MapPageState();
}

class _MapPageState extends State<MapPage> {
  late LocationPermission permission;
  late GoogleMapController mapController;
  LatLng? _currentPosition;
  bool _isLoading = true;

  @override
  void initState() {
    super.initState();
    getLocation();
  }

  getLocation() async {
    permission = await Geolocator.requestPermission();

    Position position = await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.high);
    double lat = position.latitude;
    double long = position.longitude;

    LatLng location = LatLng(lat, long);

    setState(() {
      _currentPosition = location;
      print("what is  $_currentPosition");
      _isLoading = false;
    });
  }

  // Completer<GoogleMapController> _controller = Completer();
  Set<Marker> myMarker = {};
  // late CameraPosition initialSettings;

  // @override
  // void initState() {
  //   initialSettings = CameraPosition(
  //     target: LatLng(double.parse("35.307001"), double.parse("38.075674")),
  //     zoom: 12,
  //   );
  //   super.initState();
  // }
  void _onMapCreated(GoogleMapController controller) {
    mapController = controller;
    setState(() {
      myMarker.add(Marker(
        consumeTapEvents: true,
        onTap: () async {
          try {
            if ((await maplauncher.MapLauncher.isMapAvailable(
                maplauncher.MapType.google))!) {
              await maplauncher.MapLauncher.showMarker(
                mapType: maplauncher.MapType.google,
                coords: maplauncher.Coords(
                    double.parse("37.42188"), double.parse("-122.080")),
                title: 'trip location',
              );
            }
          } catch (error) {
            print('hello world map 2 error $error');
          }
        },
        markerId: MarkerId("1"),
        position: LatLng(double.parse("37.42188"), double.parse("-122.080")),
      ));
    });
  }

  @override
  Widget build(BuildContext context) {
    return StatefulBuilder(
        builder: (BuildContext context, StateSetter setState) {
      return _isLoading
          ? Center(
              child: SizedBox(
                height: 20.h,
                width: 20.w,
                child: const LoadingIndicator(
                    indicatorType: Indicator.lineSpinFadeLoader,
                    colors: [
                      AppColors.seconedaryColor,
                      AppColors.fourthColor,
                      AppColors.primaryColor,
                      AppColors.fourthColor,
                      AppColors.thirdColor
                    ],
                    strokeWidth: 0.5,
                    backgroundColor: Colors.transparent,
                    pathBackgroundColor: Colors.transparent),
              ),
            )
          : GoogleMap(
              gestureRecognizers: <Factory<OneSequenceGestureRecognizer>>{
                Factory<OneSequenceGestureRecognizer>(
                  () => EagerGestureRecognizer(),
                ),
              },
              mapType: MapType.hybrid,
              initialCameraPosition: CameraPosition(
                target: _currentPosition!,
                zoom: 16.0,
              ),
              myLocationEnabled: true,
              onMapCreated: _onMapCreated,
              markers: myMarker,
            );
    });
  }
}
