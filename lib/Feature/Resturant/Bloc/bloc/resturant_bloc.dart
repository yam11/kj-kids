import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:junior/Core/Api/ExceptionsHandle.dart';
import 'package:junior/Core/Api/Network.dart';
import 'package:junior/Core/Api/Urls.dart';
import 'package:junior/Feature/Home/Models/food_list_model.dart';
import 'package:junior/Feature/Resturant/Models/food_details_model.dart';
import 'package:meta/meta.dart';

part 'resturant_event.dart';
part 'resturant_state.dart';

class ResturantBloc extends Bloc<ResturantEvent, ResturantState> {
  FoodListModel? foodListModel;
  FoodDetailsModel? foodDetailsModel;
  
  List<Food> drink = [];
  List<Food> meal = [];
  ResturantBloc() : super(ResturantInitial()) {
    on<ResturantEvent>((event, emit) async {
      if (event is FoodResturant) {
        emit(LoadingToGetFoodResturant());
        try {
          final response = await Network.getData(url: Urls.foodList);

          foodListModel = FoodListModel.fromJson(response.data);
       
         for (var v in foodListModel!.data!) {
            if (v.type == "drink") {
              drink.add(v);
            } else {
              meal.add(v);
            }
          }

          emit(SuccesToGetFoodResturant());
        } catch (error) {
          if (error is DioError) {
            emit(ErrorToGetFoodResturant(
                message: exceptionsHandle(error: error)));
          } else {
            emit(ErrorToGetFoodResturant(message: "error"));
          }
        }
      }
      if (event is FoodDetails) {
        emit(LoadingToGetFoodDetails());
        try {
          final response =
              await Network.getData(url: "${Urls.foodDetails}${event.foodId}");

          foodDetailsModel = FoodDetailsModel.fromJson(response.data);

          emit(SuccesToGetFoodDetails());
        } catch (error) {
          if (error is DioError) {
            emit(
                ErrorToGetFoodDetails(message: exceptionsHandle(error: error)));
          } else {
            emit(ErrorToGetFoodDetails(message: "error"));
          }
        }
      }
      if (event is MakeOrder) {
        List<Map<String, dynamic>> food = [
          {
            "id": event.foodId,
            "name": event.name,
            "type": event.type,
            "description": event.description,
            "price": event.totalPrice,
            "image": event.image
          }
        ];
        Map<String, dynamic> map = {
          "childId": event.childId,
          "totalPrice": event.totalPrice,
          "food": food
        };
        emit(LoadingToMakeOrder());
        Future.delayed(const Duration(seconds: 4));
        try {
          await Network.postData(url: Urls.order, data: map);

          emit(SuccesToMakeOrder());
        } catch (error) {
          if (error is DioError) {
            emit(ErrorToMakeOrder(message: exceptionsHandle(error: error)));
          } else {
            emit(ErrorToMakeOrder(message: "error"));
          }
        }
      }
    });
  }
}
