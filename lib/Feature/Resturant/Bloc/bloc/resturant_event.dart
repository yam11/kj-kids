part of 'resturant_bloc.dart';

@immutable
abstract class ResturantEvent {}

class FoodResturant extends ResturantEvent {}

class FoodDetails extends ResturantEvent {
  int foodId;
  FoodDetails({required this.foodId});
}

class MakeOrder extends ResturantEvent {
  final int childId;
  final int totalPrice;
  final int foodId;
  final String description;
  final String type;
  final String image;
   final String name;

  MakeOrder(
      {required this.childId,
      required this.totalPrice,
      required this.foodId,
      required this.description,
      required this.type,
      required this.image,
      required this.name});
}
