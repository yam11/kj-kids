part of 'resturant_bloc.dart';

@immutable
abstract class ResturantState {}

class ResturantInitial extends ResturantState {}

class LoadingToGetFoodResturant extends ResturantState {}

class SuccesToGetFoodResturant extends ResturantState {}

class ErrorToGetFoodResturant extends ResturantState {
  final String message;
  ErrorToGetFoodResturant({required this.message});
}

class LoadingToGetFoodDetails extends ResturantState {}

class SuccesToGetFoodDetails extends ResturantState {}

class ErrorToGetFoodDetails extends ResturantState {
  final String message;
  ErrorToGetFoodDetails({required this.message});
}

class LoadingToMakeOrder extends ResturantState {}

class SuccesToMakeOrder extends ResturantState {}

class ErrorToMakeOrder extends ResturantState {
  final String message;
  ErrorToMakeOrder({required this.message});
}
