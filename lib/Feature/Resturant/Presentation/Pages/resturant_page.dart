import 'package:animate_do/animate_do.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:junior/App/app_localizations.dart';
import 'package:junior/Core/Constants/app_assets.dart';
import 'package:junior/Core/Widgets/app_bar_widget.dart';
import 'package:junior/Core/Widgets/back_graound_widget.dart';
import 'package:junior/Core/Widgets/error_message_widget.dart';
import 'package:junior/Core/Widgets/loading_widget.dart';
import 'package:junior/Core/Widgets/tilte_page_widget.dart';
import 'package:junior/Core/Widgets/title_widget.dart';
import 'package:junior/Feature/Resturant/Bloc/bloc/resturant_bloc.dart';
import 'package:junior/Feature/Resturant/Presentation/Pages/product_details_page.dart';
import 'package:junior/Core/Widgets/carousel_slider_image.dart';
import 'package:junior/Feature/Resturant/Presentation/Widgets/drinks_page_view.dart';
import 'package:junior/Feature/Home/Presentation/Widgets/products_widget.dart';
import 'package:page_transition/page_transition.dart';
import 'package:sizer/sizer.dart';

class ResturantPage extends StatefulWidget {
  ResturantPage({Key? key}) : super(key: key);

  @override
  State<ResturantPage> createState() => _ResturantPageState();
}

class _ResturantPageState extends State<ResturantPage> {


  int activeIndex = 0;

  final controller = CarouselController();
  ResturantBloc resturantBloc = ResturantBloc();
  int lengthOfCarouselSlider = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBody: true,
      extendBodyBehindAppBar: true,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(AppBar().preferredSize.height),
        child: const AppBarWidget(),
      ),
      body: BackGraoundWidget(
          child: SafeArea(
        child: BlocProvider(
          create: (context) => resturantBloc..add(FoodResturant()),
          child: BlocConsumer<ResturantBloc, ResturantState>(
            listener: (context, state) {
              if (resturantBloc.meal.length > 2 &&
                  resturantBloc.meal.isNotEmpty) {
                lengthOfCarouselSlider = 2;
              }
            },
            builder: (context, state) {
              if (state is ErrorToGetFoodResturant) {
                return ErrorMessageWidget(
                    onPressed: () {
                      resturantBloc.add(FoodResturant());
                    },
                    message: state.message);
              }
              if (state is SuccesToGetFoodResturant) {
                return SingleChildScrollView(
                  physics: const BouncingScrollPhysics(),
                  child: Column(children: [
                    FadeInDown(
                        duration: const Duration(milliseconds: 1600),
                        child: TitlePageWidget(title: "Resturant".tr(context))),
                    SizedBox(
                      height: 4.h,
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        CarouselSlider.builder(
                            carouselController: controller,
                            itemCount: lengthOfCarouselSlider,
                            itemBuilder: (context, index, realIndex) {
                              return buildImage(
                                resturantBloc.meal,
                                index,
                              );
                            },
                            options: CarouselOptions(
                              enableInfiniteScroll: false,
                              height: 30.h,
                              autoPlay: true,
                              viewportFraction: 1,
                              onPageChanged: (index, reason) {
                                setState(() {
                                  activeIndex = index;
                                });
                              },
                            )),
                        SizedBox(
                          height: 1.h,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            buildIndicator(activeIndex, lengthOfCarouselSlider),
                          ],
                        )
                      ],
                    ),
                    SizedBox(
                      height: 2.h,
                    ),
                    TitleWidget(title: "Drinks".tr(context)),
                    SizedBox(
                      height: 2.h,
                    ),
                    DrinksPageView(resturantBloc: resturantBloc),
                    SizedBox(
                      height: 2.h,
                    ),
                    TitleWidget(title: "Food".tr(context)),
                    SizedBox(
                      height: 2.h,
                    ),
                    GridView.builder(
                      physics: const NeverScrollableScrollPhysics(),
                      shrinkWrap: true,
                      itemCount: resturantBloc.meal.length,
                      gridDelegate:
                          const SliverGridDelegateWithFixedCrossAxisCount(
                              crossAxisCount: 2,
                              crossAxisSpacing: 5,
                              childAspectRatio: 0.9,
                              mainAxisSpacing: 10),
                      itemBuilder: (context, index) {
                        return InkWell(
                          onTap: () {
                            Navigator.of(context).push(
                              PageTransition(
                                  child: ProductDetailsPage(
                                    foodId: resturantBloc.meal[index].id!,
                                  ),
                                  type: PageTransitionType.bottomToTop,
                                  duration: const Duration(milliseconds: 300)),
                            );
                          },
                          child: ProductWidget(
                              img: resturantBloc.meal[index].image!,
                              title: resturantBloc.meal[index].name!),
                        );
                      },
                    ),
                    SizedBox(
                      height: 5.h,
                    ),
                  ]),
                );
              } else {
                return const LoadingWidget();
              }
            },
          ),
        ),
      )),
    );
  }
}
