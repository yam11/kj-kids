import 'package:animate_do/animate_do.dart';
import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:junior/App/app_localizations.dart';
import 'package:junior/Core/Constants/app_assets.dart';
import 'package:junior/Core/Constants/app_colors.dart';
import 'package:junior/Core/Util/SharedPreferences/SharedPreferencesHelper.dart';
import 'package:junior/Core/Widgets/app_bar_widget.dart';
import 'package:junior/Core/Widgets/back_graound_widget.dart';
import 'package:junior/Core/Widgets/error_message_widget.dart';
import 'package:junior/Core/Widgets/loading_widget.dart';
import 'package:junior/Core/Widgets/tilte_page_widget.dart';
import 'package:junior/Core/Widgets/title_widget.dart';
import 'package:junior/Feature/Comments/Models/comments_model.dart';
import 'package:junior/Feature/Comments/Presentation/Pages/commenst_page.dart';
import 'package:junior/Feature/Comments/Presentation/Widget/comments_widget.dart';
import 'package:junior/Feature/Comments/Presentation/Widget/write_comment_widget.dart';
import 'package:junior/Feature/Resturant/Bloc/bloc/resturant_bloc.dart';
import 'package:junior/Feature/Resturant/Presentation/Widgets/pick_child_widget.dart';

import 'package:page_transition/page_transition.dart';
import 'package:sizer/sizer.dart';

class ProductDetailsPage extends StatelessWidget {
  int foodId;
  ProductDetailsPage({required this.foodId, Key? key}) : super(key: key);
  TextEditingController commentsController = TextEditingController();
  ResturantBloc resturantBloc = ResturantBloc();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBody: true,
      extendBodyBehindAppBar: true,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(AppBar().preferredSize.height),
        child: const AppBarWidget(),
      ),
      body: BackGraoundWidget(
          child: BlocProvider(
        create: (context) => resturantBloc..add(FoodDetails(foodId: foodId)),
        child: SafeArea(
          child: Stack(
            alignment: Alignment.bottomRight,
            children: [
              BlocConsumer<ResturantBloc, ResturantState>(
                listener: (context, state) {},
                builder: (context, state) {
                  if (state is ErrorToGetFoodDetails) {
                    return ErrorMessageWidget(
                        onPressed: () {
                          resturantBloc.add(FoodDetails(foodId: foodId));
                        },
                        message: state.message);
                  }
                  if (state is SuccesToGetFoodDetails ||
                      state is SuccesToMakeOrder ||
                      state is ErrorToMakeOrder ||
                      state is LoadingToMakeOrder) {
                    return Column(children: [
                      FadeInDown(
                          duration: const Duration(milliseconds: 1600),
                          child: TitlePageWidget(title: "Details".tr(context))),
                      Expanded(
                        child: SingleChildScrollView(
                          physics: const BouncingScrollPhysics(),
                          child: Column(
                            children: [
                              Container(
                                height: 25.h,
                                margin: EdgeInsets.symmetric(
                                    horizontal: 4.w, vertical: 1.h),
                                decoration: BoxDecoration(
                                    color: AppColors.whiteColor,
                                    border: Border.all(
                                        color: AppColors.grayColor, width: 2),
                                    borderRadius: BorderRadius.circular(5.w),
                                    image: DecorationImage(
                                        image: NetworkImage(resturantBloc
                                            .foodDetailsModel!.data!.image!),
                                        fit: BoxFit.fill)),
                              ),
                              Text(
                                resturantBloc.foodDetailsModel!.data!.name!,
                                style: TextStyle(
                                    color: AppColors.primaryColor,
                                    fontSize: 18.sp,
                                    fontWeight: FontWeight.bold),
                              ),
                              SizedBox(
                                height: 1.h,
                              ),
                              TitleWidget(
                                title: "Food content".tr(context),
                              ),
                              SizedBox(
                                height: 1.h,
                              ),
                              Padding(
                                padding: EdgeInsets.symmetric(horizontal: 5.w),
                                child: Text(
                                  resturantBloc
                                      .foodDetailsModel!.data!.description!,
                                  textAlign: TextAlign.start,
                                  style: TextStyle(
                                      color: AppColors.seconedaryColor,
                                      fontSize: 14.sp,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                              SizedBox(
                                height: 1.h,
                              ),
                              TitleWidget(
                                title: "Price".tr(context),
                              ),
                              SizedBox(
                                height: 1.h,
                              ),
                              Padding(
                                padding: EdgeInsets.symmetric(horizontal: 5.w),
                                child: Row(
                                  children: [
                                    Text(
                                      "${resturantBloc.foodDetailsModel!.data!.price!}",
                                      textAlign: TextAlign.start,
                                      style: TextStyle(
                                          color: AppColors.seconedaryColor,
                                          fontSize: 14.sp,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ],
                                ),
                              ),
                              SizedBox(
                                height: 2.h,
                              ),
                              TitleWidget(
                                  title: "Share your opinion about food"
                                      .tr(context)),
                              SizedBox(
                                height: 2.h,
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  TextButton(
                                      onPressed: () {
                                        Navigator.of(context).push(
                                          PageTransition(
                                              child: CommentsPage(
                                                  commendModel: commendForFood),
                                              type: PageTransitionType
                                                  .rightToLeft,
                                              duration: const Duration(
                                                  milliseconds: 300)),
                                        );
                                      },
                                      child: Row(
                                        children: [
                                          Text(
                                            "All Opinion".tr(context),
                                            style: TextStyle(
                                                color:
                                                    AppColors.seconedaryColor,
                                                fontSize: 14.sp,
                                                fontWeight: FontWeight.bold),
                                          ),
                                          Icon(
                                            Icons.arrow_forward,
                                            color: AppColors.seconedaryColor,
                                            size: 14.sp,
                                          )
                                        ],
                                      )),
                                ],
                              ),
                              for (int i = 0; i < 2; i++)
                                CommentsWidget(
                                  avatarImage: AppAssets.womanIcon,
                                  commendModel: commendForFood,
                                  index: i,
                                ),
                              WriteCommentsWidget(
                                type: "food",
                              ),
                              SizedBox(
                                height: 10.h,
                              )
                            ],
                          ),
                        ),
                      )
                    ]);
                  } else {
                    return const LoadingWidget();
                  }
                },
              ),
              BlocConsumer<ResturantBloc, ResturantState>(
                listener: (context, state) {
                  if (state is ErrorToMakeOrder) {
                    print("what is ${state.message}");
                  }
                  if (state is SuccesToMakeOrder) {
                    AwesomeDialog(
                      title:
                          "The process of adding the order was completed successfully"
                              .tr(context),
                      titleTextStyle: TextStyle(
                          color: AppColors.primaryColor,
                          fontSize: 16.sp,
                          fontWeight: FontWeight.bold),
                      dialogBackgroundColor: Colors.white,
                      btnOkColor: AppColors.primaryColor,
                      btnOkOnPress: () {},
                      btnOkText: "Ok".tr(context),
                      dismissOnTouchOutside: true,
                      context: context,
                      dialogType: DialogType.success,
                      animType: AnimType.rightSlide,
                    ).show();
                  }
                },
                builder: (context, state) {
                  return Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          InkWell(
                            onTap: () {
                              pickChild(
                                  context,
                                  resturantBloc,
                                  resturantBloc.foodDetailsModel!.data!.name!,
                                  resturantBloc.foodDetailsModel!.data!.image!,
                                  resturantBloc.foodDetailsModel!.data!.type!,
                                  resturantBloc
                                      .foodDetailsModel!.data!.description!,
                                  resturantBloc.foodDetailsModel!.data!.id!,
                                  resturantBloc.foodDetailsModel!.data!.price!);
                            },
                            child: Container(
                              padding: EdgeInsets.symmetric(
                                  horizontal: 5.w, vertical: 2.h),
                              alignment: Alignment.center,
                              decoration: BoxDecoration(
                                  color: AppColors.primaryColor,
                                  borderRadius: !AppSharedPreferences.hasArLang
                                      ? BorderRadius.only(
                                          topLeft: Radius.circular(5.w))
                                      : BorderRadius.only(
                                          topRight: Radius.circular(5.w)),
                                  border: Border.all(
                                      width: 2,
                                      color: AppColors.seconedaryColor)),
                              child: state is! LoadingToMakeOrder
                                  ? Row(
                                      children: [
                                        Icon(
                                          Icons.shopping_cart_outlined,
                                          color: AppColors.seconedaryColor,
                                          size: 10.w,
                                        ),
                                        Text(
                                          "Order".tr(context),
                                          style: TextStyle(
                                              color: AppColors.seconedaryColor,
                                              fontSize: 18.sp,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ],
                                    )
                                  : Row(
                                      children: const [
                                        CircularProgressIndicator(
                                          color: AppColors.seconedaryColor,
                                        )
                                      ],
                                    ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  );
                },
              )
            ],
          ),
        ),
      )),
    );
  }
}
