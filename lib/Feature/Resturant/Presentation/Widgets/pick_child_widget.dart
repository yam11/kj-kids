import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:junior/Core/Constants/app_assets.dart';
import 'package:junior/Core/Constants/app_colors.dart';
import 'package:junior/Core/Widgets/error_message_widget.dart';
import 'package:junior/Core/Widgets/loading_widget.dart';
import 'package:junior/Feature/MyProfile/Bloc/bloc/profile_bloc.dart';
import 'package:junior/Feature/Resturant/Bloc/bloc/resturant_bloc.dart';
import 'package:junior/Feature/Resturant/Presentation/Widgets/card_child_widget.dart';
import 'package:sizer/sizer.dart';

pickChild(BuildContext context, ResturantBloc resturantBloc, String name,
    String image, String type, String description, int foodId, int totalPrice) {
  ProfileBloc profileBloc = ProfileBloc();
  return showModalBottomSheet(
      isScrollControlled: true,
      backgroundColor: Colors.transparent,
      barrierColor: Colors.transparent,
      context: context,
      builder: (context) {
        return Container(
            height: 50.h,
            decoration: BoxDecoration(
                color: AppColors.primaryColor,
                // image: const DecorationImage(
                //   image: AssetImage(AppAssets.homeBackGraound),
                // ),
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(5.h),
                    topRight: Radius.circular(5.h))),
            child: Column(
              children: [
                Padding(
                  padding:
                      EdgeInsets.symmetric(horizontal: 15.w, vertical: 1.h),
                  child: const Divider(
                    thickness: 2,
                    color: AppColors.seconedaryColor,
                  ),
                ),
                Expanded(
                    child: BlocProvider(
                  create: (context) => profileBloc..add(GetProfile()),
                  child: BlocConsumer<ProfileBloc, ProfileState>(
                    listener: (context, state) {},
                    builder: (context, state) {
                      if (state is ErrorToGetProfile) {
                        return ErrorMessageWidget(
                            onPressed: () {
                              profileBloc.add(GetProfile());
                            },
                            message: state.message);
                      }
                      if (state is SuccesToGetProfile) {
                        return ListView.builder(
                          physics: const BouncingScrollPhysics(),
                          itemCount:
                              profileBloc.profileModel!.data!.children!.length,
                          itemBuilder: (context, index) {
                            return CardChildWidget(
                              resturantBloc: resturantBloc,
                              childId: profileBloc
                                  .profileModel!.data!.children![index].id!,
                              description: description,
                              foodId: foodId,
                              image: image,
                              name: name,
                              type: type,
                              totalPrice: totalPrice,
                              childImage: AppAssets.profileIcon,
                              childName: profileBloc.profileModel!.data!
                                  .children![index].firstName!,
                            );
                          },
                        );
                      } else {
                        return const LoadingWidget();
                      }
                    },
                  ),
                ))
              ],
            ));
      });
}
