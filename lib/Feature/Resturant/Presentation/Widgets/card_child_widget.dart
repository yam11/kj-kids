import 'package:flutter/material.dart';
import 'package:junior/Core/Constants/app_colors.dart';
import 'package:junior/Feature/Resturant/Bloc/bloc/resturant_bloc.dart';
import 'package:sizer/sizer.dart';

class CardChildWidget extends StatelessWidget {
  final int childId;
  final int totalPrice;
  final int foodId;
  final String description;
  final String name;
  final String image;
  final String type;
  final String childName;
  final String childImage;
  ResturantBloc resturantBloc;
  CardChildWidget(
      {required this.childImage,
      required this.resturantBloc,
      required this.childName,
      Key? key,
      required this.childId,
      required this.totalPrice,
      required this.foodId,
      required this.description,
      required this.name,
      required this.image,
      required this.type})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(
        horizontal: 5.w,
        vertical: 1.h,
      ),
      decoration: BoxDecoration(boxShadow: [
        BoxShadow(
            color: Colors.grey.withOpacity(0.1),
            spreadRadius: 2,
            blurRadius: 4,
            offset: const Offset(3, 3)),
      ]),
      child: MaterialButton(
        onPressed: () {
                   Navigator.of(context).pop();
          resturantBloc.add(MakeOrder(
              childId: childId,
              totalPrice: totalPrice,
              foodId: foodId,
              description: description,
              type: type,
              image: image,
              name: name));
        },
        padding: EdgeInsets.symmetric(
          horizontal: 2.w,
          vertical: 3.h,
        ),
        minWidth: double.infinity,
        color: AppColors.whiteColor,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(5.w),
        ),
        child: Row(
          children: [
            Container(
              height: 8.h,
              width: 16.w,
              decoration: BoxDecoration(
                  image: DecorationImage(image: AssetImage(childImage))),
            ),
            SizedBox(
              width: 5.w,
            ),
            Text(
              childName,
              style: TextStyle(
                  fontSize: 15.sp,
                  fontWeight: FontWeight.bold,
                  color: AppColors.primaryColor),
            ),
          ],
        ),
      ),
    );
  }
}
