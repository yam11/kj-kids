import 'package:flutter/material.dart';
import 'package:junior/Core/Constants/app_assets.dart';
import 'package:junior/Feature/Home/Presentation/Widgets/products_widget.dart';
import 'package:junior/Feature/Resturant/Bloc/bloc/resturant_bloc.dart';
import 'package:junior/Feature/Resturant/Presentation/Pages/product_details_page.dart';
import 'package:page_transition/page_transition.dart';
import 'package:sizer/sizer.dart';

class DrinksPageView extends StatelessWidget {
  ResturantBloc resturantBloc;
  DrinksPageView({required this.resturantBloc, Key? key}) : super(key: key);
  PageController pageController =
      PageController(initialPage: 0, viewportFraction: 1);

  @override
  Widget build(BuildContext context) {
    return Container(
        height: 30.h,
        padding: EdgeInsets.symmetric(vertical: 2.h),
        alignment: Alignment.center,
        child: PageView.builder(
            physics: const BouncingScrollPhysics(),
            controller: pageController,
            itemCount: (resturantBloc.drink.length / 2).ceil(),
            itemBuilder: (context, index) {
              int inc1 = index;
              int inc2 = index + 1;
              return Row(
                children: [
                  InkWell(
                    onTap: () {
                      Navigator.of(context).push(
                        PageTransition(
                            child: ProductDetailsPage(
                              foodId: resturantBloc.drink[inc1].id!,
                            ),
                            type: PageTransitionType.bottomToTop,
                            duration: const Duration(milliseconds: 300)),
                      );
                    },
                    child: ProductWidget(
                        img: resturantBloc.drink[inc1].image!,
                        title: resturantBloc.drink[inc1].name!),
                  ),
                  if (index + inc2 < resturantBloc.drink.length)
                    InkWell(
                      onTap: () {
                        Navigator.of(context).push(
                          PageTransition(
                              child: ProductDetailsPage(
                                foodId: resturantBloc.drink[inc2].id!,
                              ),
                              type: PageTransitionType.bottomToTop,
                              duration: const Duration(milliseconds: 300)),
                        );
                      },
                      child: ProductWidget(
                          img: resturantBloc.drink[inc2].image!,
                          title: resturantBloc.drink[inc2].name!),
                    ),
                ],
              );
            }));
  }
}
