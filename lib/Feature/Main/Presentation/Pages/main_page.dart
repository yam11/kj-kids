import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:junior/App/app_localizations.dart';

import 'package:junior/Core/Constants/app_colors.dart';
import 'package:junior/Core/Widgets/back_graound_widget.dart';
import 'package:junior/Core/Widgets/error_message_widget.dart';
import 'package:junior/Core/Widgets/loading_widget.dart';
import 'package:junior/Feature/Home/Bloc/bloc/home_bloc.dart';
import 'package:junior/Feature/Map/Presentation/Pages/map_page.dart';
import 'package:junior/Feature/More/Presentation/Pages/more_page.dart';
import 'package:junior/Feature/Home/Presentation/Pages/home_page.dart';
import 'package:sizer/sizer.dart';

class MainPage extends StatelessWidget {
  HomeBloc homeBloc = HomeBloc();
  MainPage({Key? key}) : super(key: key);

  // int initialIndex = 1;
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      initialIndex: 1,
      child: Scaffold(
          extendBodyBehindAppBar: true,
          appBar: AppBar(
            centerTitle: true,
            elevation: 0,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(10.w),
                  bottomRight: Radius.circular(10.w)),
            ),
            backgroundColor: AppColors.primaryColor,
            title: Padding(
              padding: EdgeInsets.symmetric(vertical: 2.h),
              child: Text(
                "Kiddies",
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontSize: 30.sp,
                    fontWeight: FontWeight.bold,
                    color: AppColors.seconedaryColor),
              ),
            ),
            bottom: TabBar(
                padding: EdgeInsets.symmetric(vertical: 1.h, horizontal: 2.w),
                // controller: tabController,
                isScrollable: false,
                labelColor: AppColors.primaryColor,
                indicator: BoxDecoration(
                  borderRadius: BorderRadius.circular(10.w),
                  color: AppColors.whiteColor,
                ),
                splashBorderRadius: BorderRadius.circular(10.w),
                labelStyle:
                    const TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                // indicatorColor: Colors.transparent,
                unselectedLabelColor: AppColors.whiteColor,
                tabs: [
                  Tab(
                    text: "More".tr(context),
                  ),
                  Tab(
                    text: "Home".tr(context),
                  ),
                  Tab(
                    text: "Map".tr(context),
                  ),
                ]),
          ),
          body: BackGraoundWidget(
              child: BlocProvider(
            create: (context) => homeBloc..add(GetHomeInfo()),
            child: BlocConsumer<HomeBloc, HomeState>(
              listener: (context, state) {},
              builder: (context, state) {
                if (state is ErrorToGetHomePage) {
                  return ErrorMessageWidget(
                      onPressed: () {
                        homeBloc.add(GetHomeInfo());
                      },
                      message: state.message);
                }
                if (state is SuccesToGetHomePage) {
                  return TabBarView(
                    physics: const NeverScrollableScrollPhysics(),
                    // controller: tabController,
                    children: [
                      MorePage(),
                      HomePage(homeBloc: homeBloc),
                      MapPage()
                    ],
                  );
                } else {
                  return const LoadingWidget();
                }
              },
            ),
          ))),
    );
  }
}
