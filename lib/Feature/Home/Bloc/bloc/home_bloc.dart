import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:junior/Core/Api/ExceptionsHandle.dart';
import 'package:junior/Core/Api/Network.dart';
import 'package:junior/Core/Api/Urls.dart';
import 'package:junior/Feature/Home/Models/food_list_model.dart';
import 'package:junior/Feature/Teachers/Models/teacher_list_model.dart';
import 'package:meta/meta.dart';

part 'home_event.dart';
part 'home_state.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  ListOfTeacherModel? listOfTeacherModel;
  FoodListModel? foodListModel;
  

  HomeBloc() : super(HomeInitial()) {
    on<HomeEvent>((event, emit) async {
      if (event is GetHomeInfo) {
        emit(LoadingToGetHomePage());
        try {
          final response1 = await Network.getData(url: Urls.teachersList);
          final response2 = await Network.getData(url: Urls.foodList);
          listOfTeacherModel = ListOfTeacherModel.fromJson(response1.data);
          foodListModel = FoodListModel.fromJson(response2.data);
      

          emit(SuccesToGetHomePage());
        } catch (error) {
          if (error is DioError) {
            emit(ErrorToGetHomePage(message: exceptionsHandle(error: error)));
          } else {
            emit(ErrorToGetHomePage(message: "error"));
          }
        }
      }
    });
  }
}
