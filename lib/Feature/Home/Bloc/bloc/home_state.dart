part of 'home_bloc.dart';

@immutable
abstract class HomeState {}

class HomeInitial extends HomeState {}

class SuccesToGetHomePage extends HomeState {}

class ErrorToGetHomePage extends HomeState {
  final String message;
  ErrorToGetHomePage({required this.message});
}

class LoadingToGetHomePage extends HomeState {}
