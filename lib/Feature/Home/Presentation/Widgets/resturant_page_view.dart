import 'package:flutter/material.dart';
import 'package:junior/Core/Constants/app_assets.dart';
import 'package:junior/Feature/Home/Bloc/bloc/home_bloc.dart';
import 'package:junior/Feature/Home/Presentation/Widgets/products_widget.dart';
import 'package:junior/Feature/Resturant/Presentation/Pages/product_details_page.dart';
import 'package:page_transition/page_transition.dart';
import 'package:sizer/sizer.dart';

class ResturantPageView extends StatelessWidget {
  HomeBloc homeBloc;
  ResturantPageView({required this.homeBloc, Key? key}) : super(key: key);
  PageController pageController =
      PageController(initialPage: 0, viewportFraction: 1);

  @override
  Widget build(BuildContext context) {
    return Container(
        height: 30.h,
        padding: EdgeInsets.symmetric(vertical: 2.h),
        alignment: Alignment.center,
        child: PageView.builder(
            physics: const BouncingScrollPhysics(),
            controller: pageController,
            itemCount: (homeBloc.foodListModel!.data!.length / 2).ceil(),
            itemBuilder: (context, index) {
              int inc1 = index;
              int inc2 = index + 1;
              return Row(
                children: [
                  InkWell(
                      onTap: () {
                        Navigator.of(context).push(
                          PageTransition(
                              child: ProductDetailsPage(
                                foodId: homeBloc.foodListModel!.data![inc1].id!,
                          
                              ),
                              type: PageTransitionType.bottomToTop,
                              duration: const Duration(milliseconds: 300)),
                        );
                      },
                      child: ProductWidget(
                          img:  homeBloc.foodListModel!.data![inc1].image!,
                          title: homeBloc.foodListModel!.data![inc1].name!)),
                  if (index + inc2 < homeBloc.foodListModel!.data!.length)
                    InkWell(
                        onTap: () {
                          Navigator.of(context).push(
                            PageTransition(
                                child: ProductDetailsPage(
                                  foodId:
                                      homeBloc.foodListModel!.data![inc2].id!,
                                 
                                ),
                                type: PageTransitionType.bottomToTop,
                                duration: const Duration(milliseconds: 300)),
                          );
                        },
                        child: ProductWidget(
                            img:homeBloc.foodListModel!.data![inc2].image!,
                            title: homeBloc.foodListModel!.data![inc2].name!)),
                ],
              );
            }));
  }
}
