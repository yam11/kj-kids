import 'package:flutter/material.dart';
import 'package:junior/App/app_localizations.dart';
import 'package:junior/Core/Constants/app_assets.dart';
import 'package:junior/Core/Constants/app_colors.dart';
import 'package:sizer/sizer.dart';

class TeacherCardWidget extends StatelessWidget {
  String teacherName;
  String teacherImage;
  final onPressed;
  TeacherCardWidget(
      {required this.teacherName,
      required this.teacherImage,
      required this.onPressed,
      Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 4.h),
      color: Colors.transparent,
      child: Column(children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            CircleAvatar(
              radius: 8.w,
              backgroundColor: AppColors.primaryColor,
              backgroundImage:  AssetImage(teacherImage),
            ),
          ],
        ),
        SizedBox(
          height: 1.h,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              teacherName,
              style: TextStyle(
                  color: AppColors.seconedaryColor,
                  fontSize: 14.sp,
                  fontWeight: FontWeight.bold),
            ),
          ],
        ),
        SizedBox(
          height: 1.h,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            MaterialButton(
              onPressed: onPressed,
              padding: EdgeInsets.symmetric(horizontal: 2.w, vertical: 2.h),
              color: AppColors.primaryColor,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5.w)),
              child: Text(
                "Show Profile".tr(context),
                style: TextStyle(
                    color: AppColors.whiteColor,
                    fontSize: 12.sp,
                    fontWeight: FontWeight.bold),
              ),
            ),
          ],
        ),
      ]),
    );
  }
}
