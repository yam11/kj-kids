import 'package:flutter/material.dart';
import 'package:junior/Core/Constants/app_colors.dart';
import 'package:sizer/sizer.dart';

class ProductWidget extends StatelessWidget {
  String img;
  String title;

  final function;
  ProductWidget(
      {this.function, required this.img, required this.title, Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 1.w, vertical: 1.h),
      child: InkWell(
        onTap: function,
        child: Container(
          width: 48.w,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(5.w),
              color: AppColors.whiteColor,
              boxShadow: const [
                BoxShadow(
                    color: Colors.black26, blurRadius: 7, offset: Offset(0, 2))
              ]),
          child: Column(
            children: [
              Expanded(
                flex: 1,
                child: Container(
                    decoration: BoxDecoration(
                        color: Colors.transparent,
                        borderRadius: BorderRadius.circular(5.w),
                        border: Border.all(color: AppColors.grayColor),
                        image: DecorationImage(
                            image: NetworkImage(
                              img,
                            ),
                            fit: BoxFit.fill))),
              ),
              Container(
                margin: EdgeInsets.symmetric(vertical: 1.h),
                alignment: Alignment.center,
                child: Text(
                  title,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: AppColors.seconedaryColor,
                      fontWeight: FontWeight.bold,
                      fontSize: 14.sp),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
