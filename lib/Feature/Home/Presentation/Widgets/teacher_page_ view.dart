import 'package:flutter/material.dart';
import 'package:junior/Core/Constants/app_assets.dart';
import 'package:junior/Feature/Home/Bloc/bloc/home_bloc.dart';
import 'package:junior/Feature/Home/Presentation/Widgets/teacher_card_widget.dart';
import 'package:junior/Feature/Teachers/Presentation/Pages/teacher_profile_page.dart';
import 'package:page_transition/page_transition.dart';
import 'package:sizer/sizer.dart';

class TeacherPageView extends StatelessWidget {
  HomeBloc homeBloc;
  TeacherPageView({required this.homeBloc, Key? key}) : super(key: key);
  PageController pageController =
      PageController(initialPage: 0, viewportFraction: 1);

  @override
  Widget build(BuildContext context) {
    return Container(
        height: 40.h,
        padding: EdgeInsets.symmetric(vertical: 2.h),
        alignment: Alignment.center,
        child: PageView.builder(
            physics: const BouncingScrollPhysics(),
            controller: pageController,
            itemCount: ((homeBloc.listOfTeacherModel!.data!.length / 2).ceil()),
            itemBuilder: (context, index) {
              int inc1 = index;
              int inc2 = index + 1;
              return Row(
                children: [
                  Expanded(
                      child: TeacherCardWidget(
                    teacherName:
                        homeBloc.listOfTeacherModel!.data![inc1].firstName! +
                            homeBloc.listOfTeacherModel!.data![inc1].lastName!,
                    teacherImage: AppAssets.womanIcon,
                    onPressed: () {
                      Navigator.push(
                          context,
                          PageTransition(
                              child: TeacherProFile(
                                  teacherId: homeBloc
                                      .listOfTeacherModel!.data![inc1].id!),
                              type: PageTransitionType.rightToLeft,
                              duration: const Duration(milliseconds: 300)));
                    },
                  )),
                  if (index + inc2 < homeBloc.listOfTeacherModel!.data!.length)
                    Expanded(
                        child: TeacherCardWidget(
                      teacherName: homeBloc
                              .listOfTeacherModel!.data![inc2].firstName! +
                          homeBloc.listOfTeacherModel!.data![inc2].lastName!,
                      teacherImage: AppAssets.womanIcon,
                      onPressed: () {
                        Navigator.push(
                            context,
                            PageTransition(
                                child: TeacherProFile(
                                    teacherId: homeBloc
                                        .listOfTeacherModel!.data![inc2].id!),
                                type: PageTransitionType.rightToLeft,
                                duration: const Duration(milliseconds: 300)));
                      },
                    )),
                ],
              );
            }));
  }
}
