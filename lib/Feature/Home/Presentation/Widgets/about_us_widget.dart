import 'package:flutter/material.dart';
import 'package:junior/App/app_localizations.dart';
import 'package:junior/Core/Constants/app_assets.dart';
import 'package:junior/Core/Constants/app_colors.dart';
import 'package:sizer/sizer.dart';

class AboutUsWidget extends StatelessWidget {
  const AboutUsWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 2.w),
            child: Text(
              "About Us Details".tr(context),
              style: TextStyle(
                color: AppColors.primaryColor,
                fontSize: 14.sp,
              ),
            ),
          ),
        ),
        Expanded(
          child: Container(
            height: 30.h,
            decoration: const BoxDecoration(
                image: DecorationImage(
                    image: AssetImage(AppAssets.aboutUsImage),
                    fit: BoxFit.fill)),
          ),
        )
      ],
    );
  }
}
