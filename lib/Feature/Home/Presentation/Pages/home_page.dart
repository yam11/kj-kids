import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:junior/App/app_localizations.dart';
import 'package:junior/Core/Constants/app_assets.dart';
import 'package:junior/Core/Constants/app_colors.dart';
import 'package:junior/Core/Widgets/error_message_widget.dart';
import 'package:junior/Core/Widgets/loading_widget.dart';
import 'package:junior/Core/Widgets/title_widget.dart';
import 'package:junior/Feature/Comments/Models/comments_model.dart';
import 'package:junior/Feature/Comments/Presentation/Pages/commenst_page.dart';
import 'package:junior/Feature/Home/Bloc/bloc/home_bloc.dart';
import 'package:junior/Feature/Resturant/Presentation/Pages/resturant_page.dart';
import 'package:junior/Feature/Teachers/Presentation/Pages/teachers_page.dart';
import 'package:junior/Feature/Home/Presentation/Widgets/about_us_widget.dart';
import 'package:junior/Feature/Comments/Presentation/Widget/comments_widget.dart';
import 'package:junior/Feature/Home/Presentation/Widgets/products_widget.dart';
import 'package:junior/Feature/Home/Presentation/Widgets/resturant_page_view.dart';
import 'package:junior/Feature/Home/Presentation/Widgets/teacher_card_widget.dart';
import 'package:junior/Feature/Home/Presentation/Widgets/teacher_page_%20view.dart';
import 'package:junior/Feature/Comments/Presentation/Widget/write_comment_widget.dart';
import 'package:page_transition/page_transition.dart';
import 'package:sizer/sizer.dart';

class HomePage extends StatelessWidget {
  HomeBloc homeBloc;
  HomePage({required this.homeBloc, Key? key}) : super(key: key);
  TextEditingController commentsController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: SingleChildScrollView(
      physics: const BouncingScrollPhysics(),
      child: Column(children: [
        SizedBox(
          height: 2.h,
        ),
        TitleWidget(title: "About Us".tr(context)),
        SizedBox(
          height: 2.h,
        ),
        const AboutUsWidget(),
        SizedBox(
          height: 5.h,
        ),
        TitleWidget(title: "Our Teacher".tr(context)),
        SizedBox(
          height: 2.h,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            TextButton(
                onPressed: () {
                  Navigator.of(context).push(
                    PageTransition(
                        child: TeacherPage(),
                        type: PageTransitionType.rightToLeft,
                        duration: const Duration(milliseconds: 300)),
                  );
                },
                child: Row(
                  children: [
                    Text(
                      "All Teacher".tr(context),
                      style: TextStyle(
                          color: AppColors.seconedaryColor,
                          fontSize: 14.sp,
                          fontWeight: FontWeight.bold),
                    ),
                    Icon(
                      Icons.arrow_forward,
                      color: AppColors.seconedaryColor,
                      size: 14.sp,
                    )
                  ],
                )),
          ],
        ),
        TeacherPageView(homeBloc: homeBloc),
        TitleWidget(title: "Resturant".tr(context)),
        SizedBox(
          height: 2.h,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            TextButton(
                onPressed: () {
                  Navigator.of(context).push(
                    PageTransition(
                        child: ResturantPage(),
                        type: PageTransitionType.rightToLeft,
                        duration: const Duration(milliseconds: 300)),
                  );
                },
                child: Row(
                  children: [
                    Text(
                      "Go to Resturant".tr(context),
                      style: TextStyle(
                          color: AppColors.seconedaryColor,
                          fontSize: 14.sp,
                          fontWeight: FontWeight.bold),
                    ),
                    Icon(
                      Icons.arrow_forward,
                      color: AppColors.seconedaryColor,
                      size: 14.sp,
                    )
                  ],
                )),
          ],
        ),
        ResturantPageView(homeBloc: homeBloc),
        SizedBox(
          height: 5.h,
        ),
        TitleWidget(title: "Share your opinion".tr(context)),
        SizedBox(
          height: 2.h,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            TextButton(
                onPressed: () {
                  Navigator.of(context).push(
                    PageTransition(
                        child: CommentsPage(
                          commendModel: commend,
                        ),
                        type: PageTransitionType.rightToLeft,
                        duration: const Duration(milliseconds: 300)),
                  );
                },
                child: Row(
                  children: [
                    Text(
                      "All Opinion".tr(context),
                      style: TextStyle(
                          color: AppColors.seconedaryColor,
                          fontSize: 14.sp,
                          fontWeight: FontWeight.bold),
                    ),
                    Icon(
                      Icons.arrow_forward,
                      color: AppColors.seconedaryColor,
                      size: 14.sp,
                    )
                  ],
                )),
          ],
        ),
        for (int i = 0; i < 2; i++)
          CommentsWidget(
            avatarImage: AppAssets.womanIcon,
            commendModel: commend,
            index: i,
          ),
        WriteCommentsWidget(
          type: "general",
        ),
        SizedBox(
          height: 5.h,
        )
      ]),
    ));
  }
}
