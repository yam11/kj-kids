class FoodListModel {
  List<Food>? data;

  FoodListModel({this.data});

  FoodListModel.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = <Food>[];
      json['data'].forEach((v) {
        data!.add(new Food.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Food {
  int? id;
  String? name;
  String? description;
  String? type;
  int? price;
  String? image;

  Food(
      {this.id,
      this.name,
      this.description,
      this.type,
      this.price,
      this.image});

  Food.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    description = json['description'];
    type = json['type'];
    price = json['price'];
    image = json['image'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['description'] = this.description;
    data['type'] = this.type;
    data['price'] = this.price;
    data['image'] = this.image;
    return data;
  }
}
