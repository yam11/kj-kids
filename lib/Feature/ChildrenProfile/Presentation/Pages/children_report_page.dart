import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:junior/App/app_localizations.dart';
import 'package:junior/Core/Constants/app_colors.dart';
import 'package:junior/Core/Widgets/app_bar_widget.dart';
import 'package:junior/Core/Widgets/back_graound_widget.dart';
import 'package:junior/Core/Widgets/tilte_page_widget.dart';
import 'package:junior/Core/Widgets/title_widget.dart';
import 'package:junior/Feature/ChildrenProfile/Bloc/children_profile_bloc/children_profile_bloc.dart';
import 'package:junior/Feature/ChildrenProfile/Presentation/Widgets/child_report_widget.dart';
import 'package:sizer/sizer.dart';
import 'package:intl/intl.dart';
import 'package:pie_chart/pie_chart.dart';

class ChildrenReportPage extends StatelessWidget {
  ChildrenProfileBloc childrenProfileBloc;
  ChildrenReportPage({required this.childrenProfileBloc, Key? key})
      : super(key: key);
  Map<String, double>? dataMap;
  List<Color> colorList = [
    AppColors.primaryColor.withOpacity(0.5),
    AppColors.seconedaryColor
  ];
  final DateFormat formatter = DateFormat('yyyy-MM-dd');
  @override
  Widget build(BuildContext context) {
    dataMap = {
      'dd': 100.0 - 5.0,
      'yy': 5.toDouble(),
    };
    return Scaffold(
      extendBody: true,
      extendBodyBehindAppBar: true,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(AppBar().preferredSize.height),
        child: const AppBarWidget(),
      ),
      body: BackGraoundWidget(
          child: SafeArea(
              child: SingleChildScrollView(
        physics: const BouncingScrollPhysics(),
        child: Column(
          children: [
            FadeInDown(
              duration: const Duration(milliseconds: 1600),
              child: TitlePageWidget(
                title: "Reports".tr(context),
              ),
            ),
            SizedBox(
              height: 1.h,
            ),
            PieChart(
              dataMap: dataMap!,
              animationDuration: const Duration(seconds: 1),
              // chartLegendSpacing: 32,
              chartRadius: 50.w,
              colorList: colorList,
              initialAngleInDegree: 0,
              chartType: ChartType.disc,
              // ringStrokeWidth: 10,
              // centerText: "HYBRID",
              legendOptions: const LegendOptions(
                // showLegendsInRow: false,
                // legendPosition: LegendPosition.left,
                showLegends: false,
                // legendShape: BoxShape.circle,
                // legendTextStyle: TextStyle(
                //   fontWeight: FontWeight.bold,
                // ),
              ),
              chartValuesOptions: const ChartValuesOptions(
                showChartValueBackground: true,
                showChartValues: true,
                showChartValuesInPercentage: true,
                showChartValuesOutside: false,
                decimalPlaces: 1,
              ),
              // gradientList: ---To add gradient colors---
              // emptyColorGradient: ---Empty Color gradient---
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 2.h, vertical: 2.w),
              margin: EdgeInsets.symmetric(horizontal: 2.w, vertical: 1.h),
              decoration: BoxDecoration(
                  color: AppColors.fourthColor.withOpacity(0.5),
                  borderRadius: BorderRadius.circular(5.w),
                  border: Border.all(color: AppColors.primaryColor, width: 1)),
              child: Column(children: [
                Row(
                  children: [
                    Container(
                      height: 4.h,
                      width: 4.w,
                      decoration: const BoxDecoration(
                          color: AppColors.seconedaryColor,
                          shape: BoxShape.circle),
                    ),
                    SizedBox(
                      width: 2.w,
                    ),
                    Expanded(
                      child: Text(
                        "The child interaction rate during the class"
                            .tr(context),
                        style: TextStyle(
                            fontSize: 12.sp,
                            color: AppColors.seconedaryColor,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ],
                ),
                Row(
                  children: [
                    Container(
                      height: 4.h,
                      width: 4.w,
                      decoration: const BoxDecoration(
                          color: AppColors.primaryColor,
                          shape: BoxShape.circle),
                    ),
                    SizedBox(
                      width: 2.w,
                    ),
                    Expanded(
                      child: Text(
                        "The percentage of the child's lack of interaction during the class"
                            .tr(context),
                        style: TextStyle(
                            fontSize: 12.sp,
                            color: AppColors.primaryColor,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ],
                ),
              ]),
            ),
            childrenProfileBloc.childernDetailsModel!.data!.reports!.isNotEmpty
                ? ListView.builder(
                    shrinkWrap: true,
                    physics: const NeverScrollableScrollPhysics(),
                    itemCount: childrenProfileBloc
                        .childernDetailsModel!.data!.reports!.length,
                    itemBuilder: (context, index) {
                      return FadeInUpBig(
                        duration: const Duration(milliseconds: 1600),
                        child: ChildReportWidget(
                          hour: childrenProfileBloc.childernDetailsModel!.data!
                              .reports![index].createdAt!
                              .replaceAll("T", " | ")
                              .replaceAll('.000000Z', ""),
                          note: childrenProfileBloc.childernDetailsModel!.data!
                              .reports![index].description!,
                        ),
                      );
                    },
                  )
                : Container(
                    padding:
                        EdgeInsets.symmetric(horizontal: 2.h, vertical: 2.w),
                    margin:
                        EdgeInsets.symmetric(horizontal: 10.w, vertical: 10.h),
                    decoration: BoxDecoration(
                        color: AppColors.fourthColor.withOpacity(0.5),
                        borderRadius: BorderRadius.circular(5.w),
                        border: Border.all(
                            color: AppColors.primaryColor, width: 1)),
                    child: Column(children: [
                      Row(
                        children: [
                          Expanded(
                            child: Text(
                              "The child does not have reports".tr(context),
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  fontSize: 15.sp,
                                  color: AppColors.seconedaryColor,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        ],
                      ),
                    ]),
                  ),
          ],
        ),
      ))),
    );
  }
}
