import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:junior/App/app_localizations.dart';
import 'package:junior/Core/Constants/app_assets.dart';
import 'package:junior/Core/Widgets/app_bar_widget.dart';
import 'package:junior/Core/Widgets/back_graound_widget.dart';
import 'package:junior/Core/Widgets/error_message_widget.dart';
import 'package:junior/Core/Widgets/loading_widget.dart';
import 'package:junior/Core/Widgets/tilte_page_widget.dart';
import 'package:junior/Feature/ChildrenProfile/Bloc/children_profile_bloc/children_profile_bloc.dart';
import 'package:junior/Feature/Home/Presentation/Widgets/teacher_card_widget.dart';
import 'package:junior/Feature/Teachers/Presentation/Pages/teacher_profile_page.dart';
import 'package:page_transition/page_transition.dart';

class ChildrenTeacherPage extends StatelessWidget {
  int classRoomId;
  ChildrenTeacherPage({required this.classRoomId, Key? key}) : super(key: key);
  ChildrenProfileBloc childrenProfileBloc = ChildrenProfileBloc();
  @override
  Widget build(BuildContext context) {
    print("whhhhhaaaattt$classRoomId");
    return Scaffold(
      extendBody: true,
      extendBodyBehindAppBar: true,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(AppBar().preferredSize.height),
        child: const AppBarWidget(),
      ),
      body: BackGraoundWidget(
          child: SafeArea(
        child: Column(children: [
          FadeInDown(
              duration: const Duration(milliseconds: 1600),
              child: TitlePageWidget(title: "Children teacher".tr(context))),
          Expanded(
            child: BlocProvider(
              create: (context) => childrenProfileBloc
                ..add(ChildrenTeacher(classRoomId: classRoomId)),
              child: BlocConsumer<ChildrenProfileBloc, ChildrenProfileState>(
                listener: (context, state) {},
                builder: (context, state) {
                  if (state is ErrorToGetChildrenTeacher) {
                    return ErrorMessageWidget(
                        onPressed: () {
                          childrenProfileBloc
                              .add(ChildrenTeacher(classRoomId: classRoomId));
                        },
                        message: state.message);
                  }
                  if (state is SuccesToGetChildrenTeacher) {
                    return Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        TeacherCardWidget(
                          teacherName: childrenProfileBloc
                                  .classRoomModel!.data!.teacher!.firstName! +
                              childrenProfileBloc
                                  .classRoomModel!.data!.teacher!.lastName!,
                          teacherImage: AppAssets.womanIcon,
                          onPressed: () {
                            Navigator.push(
                                context,
                                PageTransition(
                                    child: TeacherProFile(
                                        teacherId: childrenProfileBloc
                                            .classRoomModel!
                                            .data!
                                            .teacher!
                                            .id!),
                                    type: PageTransitionType.rightToLeft,
                                    duration:
                                        const Duration(milliseconds: 300)));
                          },
                        ),
                      ],
                    );
                  } else {
                    return const LoadingWidget();
                  }
                },
              ),
            ),
          )
        ]),
      )),
    );
  }
}
