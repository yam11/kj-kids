import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:junior/App/app_localizations.dart';
import 'package:junior/Core/Constants/app_colors.dart';
import 'package:junior/Core/Widgets/app_bar_widget.dart';
import 'package:junior/Core/Widgets/back_graound_widget.dart';
import 'package:junior/Core/Widgets/error_message_widget.dart';
import 'package:junior/Core/Widgets/loading_widget.dart';
import 'package:junior/Core/Widgets/tilte_page_widget.dart';
import 'package:junior/Feature/ChildrenProfile/Bloc/child_home_work_bloc/child_home_work_bloc.dart';
import 'package:junior/Feature/ChildrenProfile/Presentation/Widgets/child_homework_widget.dart';
import 'package:sizer/sizer.dart';

class ChildrenHomeWorkPage extends StatelessWidget {
  ChildrenHomeWorkPage({Key? key}) : super(key: key);
  ChildHomeWorkBloc childHomeWorkBloc = ChildHomeWorkBloc();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBody: true,
      extendBodyBehindAppBar: true,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(AppBar().preferredSize.height),
        child: const AppBarWidget(),
      ),
      body: BackGraoundWidget(
          child: BlocProvider(
        create: (context) => childHomeWorkBloc..add(ChildHomeWork()),
        child: SafeArea(
          child: Column(
            children: [
              FadeInDown(
                duration: const Duration(milliseconds: 1600),
                child: TitlePageWidget(
                  title: "HomeWorks".tr(context),
                ),
              ),
              Expanded(
                  child: BlocConsumer<ChildHomeWorkBloc, ChildHomeWorkState>(
                listener: (context, state) {
                  if (state is SuccessSendHomeWork) {
                    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                      duration: const Duration(seconds: 2),
                      content: Row(
                        children: [
                          Text(
                            "Succes to Send HomeWork",
                            style: TextStyle(
                                color: AppColors.whiteColor, fontSize: 11.sp),
                          ),
                          SizedBox(
                            width: 4.w,
                          ),
                        ],
                      ),
                      backgroundColor: (Colors.green),
                    ));
                  }
                },
                builder: (context, state) {
                  if (state is ErrorToGetChildHomeWork) {
                    return ErrorMessageWidget(
                        onPressed: () {}, message: state.message);
                  }
                  if (state is! ErrorToGetChildHomeWork &&
                      state is! LoadingChildHomeWork) {
                    return childHomeWorkBloc.homeWork.isNotEmpty
                        ? ListView.builder(
                            physics: const BouncingScrollPhysics(),
                            itemCount: childHomeWorkBloc.homeWork.length,
                            itemBuilder: (context, index) {
                              return ChildHomeWorkWidget(
                                title: childHomeWorkBloc.homeWork[index].name!,
                                homeWork: childHomeWorkBloc
                                    .homeWork[index].description!,
                                onPressed: () {
                                  childHomeWorkBloc
                                      .add(SendHomeWork(index: index));
                                },
                              );
                            },
                          )
                        : Container(
                            padding: EdgeInsets.symmetric(
                                horizontal: 2.h, vertical: 2.w),
                            margin: EdgeInsets.symmetric(
                                horizontal: 10.w, vertical: 10.h),
                            decoration: BoxDecoration(
                                color: AppColors.fourthColor.withOpacity(0.5),
                                borderRadius: BorderRadius.circular(5.w),
                                border: Border.all(
                                    color: AppColors.primaryColor, width: 1)),
                            child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Row(
                                    children: [
                                      Expanded(
                                        child: Text(
                                          "The child does not have HomeWork",
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                              fontSize: 15.sp,
                                              color: AppColors.seconedaryColor,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ),
                                    ],
                                  ),
                                ]),
                          );
                  } else {
                    return const LoadingWidget();
                  }
                },
              ))
            ],
          ),
        ),
      )),
    );
  }
}
