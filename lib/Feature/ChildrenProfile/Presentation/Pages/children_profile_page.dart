import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:junior/App/app_localizations.dart';
import 'package:junior/Core/Constants/app_assets.dart';
import 'package:junior/Core/Constants/app_colors.dart';
import 'package:junior/Core/Widgets/app_bar_widget.dart';
import 'package:junior/Core/Widgets/back_graound_widget.dart';
import 'package:junior/Core/Widgets/error_message_widget.dart';
import 'package:junior/Core/Widgets/loading_widget.dart';
import 'package:junior/Core/Widgets/tilte_page_widget.dart';
import 'package:junior/Feature/ChildrenProfile/Bloc/children_profile_bloc/children_profile_bloc.dart';
import 'package:junior/Feature/ChildrenProfile/Presentation/Pages/children_details_page.dart';
import 'package:junior/Feature/ChildrenProfile/Presentation/Widgets/child_profile_card_widget.dart';
import 'package:junior/Feature/MyProfile/Bloc/bloc/profile_bloc.dart';
import 'package:page_transition/page_transition.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:shimmer/shimmer.dart';
import 'package:sizer/sizer.dart';

class ChildrenProfile extends StatelessWidget {
  ChildrenProfile({Key? key}) : super(key: key);
  ProfileBloc profileBloc = ProfileBloc();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBody: true,
      extendBodyBehindAppBar: true,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(AppBar().preferredSize.height),
        child: const AppBarWidget(),
      ),
      body: BackGraoundWidget(
          child: SafeArea(
        child: Column(
          children: [
            FadeInDown(
              duration: const Duration(milliseconds: 1600),
              child: TitlePageWidget(
                title: "My Children".tr(context),
              ),
            ),
            Expanded(
              child: BlocProvider(
                create: (context) => profileBloc..add(GetProfile()),
                child: BlocConsumer<ProfileBloc, ProfileState>(
                  listener: (context, state) {},
                  builder: (context, state) {
                    if (state is ErrorToGetProfile) {
                      return ErrorMessageWidget(
                          onPressed: () {
                            profileBloc.add(GetProfile());
                          },
                          message: state.message);
                    }
                    if (state is SuccesToGetProfile ||
                        state is SuccesToDeleteOrder ||
                        state is LoadingToDeleteOrder) {
                      return AnimationLimiter(
                        child: ListView.builder(
                          physics: const BouncingScrollPhysics(
                              parent: AlwaysScrollableScrollPhysics()),
                          itemCount:
                              profileBloc.profileModel!.data!.children!.length,
                          itemBuilder: (context, index) {
                            return AnimationConfiguration.staggeredList(
                              position: index,
                              delay: const Duration(milliseconds: 100),
                              child: SlideAnimation(
                                duration: const Duration(milliseconds: 1800),
                                child: FadeInAnimation(
                                  curve: Curves.fastLinearToSlowEaseIn,
                                  child: ChildCardWidget(
                                    avatarImage: AppAssets.profileIcon,
                                    name: profileBloc.profileModel!.data!
                                        .children![index].firstName!,
                                    illness: "illness",
                                    state: profileBloc.profileModel!.data!
                                        .children![index].status!,
                                    onPressed: () {
                                      Navigator.push(
                                          context,
                                          PageTransition(
                                              child: ChildrenDetailsPage(
                                                  childId: profileBloc
                                                      .profileModel!
                                                      .data!
                                                      .children![index]
                                                      .id!,
                                                  classRoomId: profileBloc
                                                      .profileModel!
                                                      .data!
                                                      .children![index]
                                                      .classroomId!,
                                                  childName: profileBloc
                                                      .profileModel!
                                                      .data!
                                                      .children![index]
                                                      .firstName!),
                                              type: PageTransitionType
                                                  .rightToLeft,
                                              duration: const Duration(
                                                  milliseconds: 300)));
                                    },
                                  ),
                                ),
                              ),
                            );
                          },
                        ),
                      );
                    } else {
                      return Shimmer.fromColors(
                          baseColor: AppColors.seconedaryColor.withOpacity(0.5),
                          highlightColor: Colors.grey[100]!,
                          enabled: true,
                          child: ListView.builder(
                            physics: const BouncingScrollPhysics(
                                parent: AlwaysScrollableScrollPhysics()),
                            padding: EdgeInsets.symmetric(vertical: 2.h),
                            itemCount: 3,
                            itemBuilder: (context, index) {
                              return Stack(
                                children: [
                                  Container(
                                    height: 15.h,
                                    margin: EdgeInsets.symmetric(
                                        horizontal: 5.w, vertical: 5.h),
                                    decoration: BoxDecoration(
                                        border: Border.all(
                                            width: 2,
                                            color: AppColors.primaryColor),
                                        color: AppColors.fourthColor
                                            .withOpacity(0.5),
                                        borderRadius:
                                            BorderRadius.circular(5.w)),
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      CircleAvatar(
                                        radius: 9.w,
                                        backgroundColor: AppColors.fourthColor
                                            .withOpacity(0.5),
                                      ),
                                    ],
                                  ),
                                ],
                              );
                            },
                          ));
                    }
                  },
                ),
              ),
            ),
          ],
        ),
      )),
    );
  }
}
