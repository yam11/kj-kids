import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:junior/App/app_localizations.dart';
import 'package:junior/Core/Constants/app_colors.dart';
import 'package:junior/Core/Widgets/error_message_widget.dart';
import 'package:junior/Core/Widgets/loading_widget.dart';
import 'package:junior/Feature/ChildrenProfile/Bloc/view_order_bloc/view_order_bloc.dart';

import 'package:sizer/sizer.dart';

viewOrder(BuildContext context, int orderId) {
  ViewOrderBloc viewOrderBloc = ViewOrderBloc();
  return showModalBottomSheet(
      isScrollControlled: true,
      backgroundColor: Colors.transparent,
      barrierColor: Colors.transparent,
      context: context,
      builder: (context) {
        return Container(
            height: 50.h,
            decoration: BoxDecoration(
                color: AppColors.seconedaryColor,
                // image: const DecorationImage(
                //   image: AssetImage(AppAssets.homeBackGraound),
                // ),
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(5.h),
                    topRight: Radius.circular(5.h))),
            child: Column(
              children: [
                Padding(
                  padding:
                      EdgeInsets.symmetric(horizontal: 15.w, vertical: 1.h),
                  child: const Divider(
                    thickness: 3,
                    color: AppColors.primaryColor,
                  ),
                ),
                Expanded(
                    child: BlocProvider(
                  create: (context) =>
                      viewOrderBloc..add(ViewOrder(orderId: orderId)),
                  child: BlocConsumer<ViewOrderBloc, ViewOrderState>(
                    listener: (context, state) {},
                    builder: (context, state) {
                      if (state is ErrorToViewOrder) {
                        return ErrorMessageWidget(
                            onPressed: () {
                              viewOrderBloc.add(ViewOrder(orderId: orderId));
                            },
                            message: state.message);
                      }
                      if (state is SuccrsToViewOrder) {
                        return Container(
                          padding: EdgeInsets.symmetric(
                              horizontal: 2.w, vertical: 2.h),
                          margin: EdgeInsets.symmetric(
                              horizontal: 2.w, vertical: 1.h),
                          decoration: BoxDecoration(
                              color: AppColors.primaryColor.withOpacity(0.5),
                              borderRadius: BorderRadius.circular(5.w),
                              border: Border.all(
                                  width: 2, color: AppColors.primaryColor)),
                          child: Column(
                            children: [
                              Row(
                                children: [
                                  Text(
                                    "Name".tr(context),
                                    style: TextStyle(
                                        color: AppColors.primaryColor,
                                        fontSize: 18.sp,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  Expanded(
                                    child: Text(
                                      viewOrderBloc.viewOrderModel!.data!
                                          .items![0].name!,
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                        color: AppColors.primaryColor,
                                        fontSize: 16.sp,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              Row(
                                children: [
                                  Text(
                                    "Description".tr(context),
                                    style: TextStyle(
                                        color: AppColors.primaryColor,
                                        fontSize: 18.sp,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ],
                              ),
                              Row(
                                children: [
                                  Expanded(
                                    child: Text(
                                      viewOrderBloc.viewOrderModel!.data!
                                          .items![0].description!,
                                      style: TextStyle(
                                        color: AppColors.primaryColor,
                                        fontSize: 16.sp,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              Text(
                                "Price".tr(context),
                                style: TextStyle(
                                    color: AppColors.primaryColor,
                                    fontSize: 18.sp,
                                    fontWeight: FontWeight.bold),
                              ),
                              Text(
                                viewOrderBloc.viewOrderModel!.data!.totalPrice
                                    .toString(),
                                style: TextStyle(
                                    color: AppColors.primaryColor,
                                    fontSize: 18.sp,
                                    fontWeight: FontWeight.bold),
                              ),
                            ],
                          ),
                        );
                      } else {
                        return const LoadingWidget();
                      }
                    },
                  ),
                ))
              ],
            ));
      });
}
