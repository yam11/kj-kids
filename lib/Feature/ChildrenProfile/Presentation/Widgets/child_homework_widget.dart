import 'dart:io';

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:junior/App/app_localizations.dart';
import 'package:junior/Core/Constants/app_colors.dart';
import 'package:junior/Core/Widgets/pick_image.dart';
import 'package:sizer/sizer.dart';

class ChildHomeWorkWidget extends StatefulWidget {
  String title;
  String homeWork;
  final onPressed;
  ChildHomeWorkWidget(
      {required this.title,
      required this.homeWork,
      required this.onPressed,
      Key? key})
      : super(key: key);

  @override
  State<ChildHomeWorkWidget> createState() => _ChildHomeWorkWidgetState();
}

class _ChildHomeWorkWidgetState extends State<ChildHomeWorkWidget> {
  String? selctedImage;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 2.w, vertical: 2.h),
      margin: EdgeInsets.symmetric(horizontal: 2.w, vertical: 1.h),
      decoration: BoxDecoration(
          color: AppColors.primaryColor.withOpacity(0.5),
          borderRadius: BorderRadius.circular(5.w),
          border: Border.all(width: 2, color: AppColors.primaryColor)),
      child: Column(
        children: [
          Row(
            children: [
              Text(
                "HomeWork".tr(context),
                style: TextStyle(
                    color: AppColors.primaryColor,
                    fontSize: 18.sp,
                    fontWeight: FontWeight.bold),
              ),
              Expanded(
                child: Text(
                  widget.title,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: AppColors.seconedaryColor,
                    fontSize: 16.sp,
                  ),
                ),
              ),
            ],
          ),
          Row(
            children: [
              Text(
                "Description".tr(context),
                style: TextStyle(
                    color: AppColors.primaryColor,
                    fontSize: 18.sp,
                    fontWeight: FontWeight.bold),
              ),
            ],
          ),
          Row(
            children: [
              Expanded(
                child: Text(
                  widget.homeWork,
                  style: TextStyle(
                    color: AppColors.seconedaryColor,
                    fontSize: 16.sp,
                  ),
                ),
              ),
            ],
          ),
          Text(
            "HomeWork Image".tr(context),
            style: TextStyle(
                color: AppColors.primaryColor,
                fontSize: 18.sp,
                fontWeight: FontWeight.bold),
          ),
          InkWell(
            onTap: () {
              AwesomeDialog(
                  dialogBackgroundColor: Colors.white,
                  context: context,
                  dialogType: DialogType.noHeader,
                  animType: AnimType.rightSlide,
                  body: PickImage(onImagePicked: (imagePath) {
                    // widget.function(File(imagePath));
                    setState(() {
                      selctedImage = imagePath;
                    });
                  })).show();
            },
            child: Container(
              alignment: Alignment.center,
              padding: EdgeInsets.symmetric(horizontal: 2.w),
              height: 20.h,
              decoration: BoxDecoration(
                image: selctedImage == null
                    ? null
                    : DecorationImage(
                        fit: BoxFit.cover,
                        image: FileImage(File(selctedImage!))),
                color: AppColors.grayColor.withOpacity(0.5),
              ),
              child: selctedImage == null
                  ? Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.add_circle_outline_outlined,
                          color: Colors.white,
                          size: 25.sp,
                        )
                      ],
                    )
                  : null,
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              MaterialButton(
                color: AppColors.primaryColor,
                padding: EdgeInsets.symmetric(horizontal: 2.w, vertical: 1.h),
                onPressed: widget.onPressed,
                child: Text(
                  "Send".tr(context),
                  style: TextStyle(
                      color: AppColors.seconedaryColor,
                      fontSize: 14.sp,
                      fontWeight: FontWeight.bold),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
