import 'package:flutter/material.dart';
import 'package:junior/App/app_localizations.dart';
import 'package:junior/Core/Constants/app_assets.dart';
import 'package:junior/Core/Constants/app_colors.dart';
import 'package:junior/Core/Widgets/title_widget.dart';
import 'package:junior/Feature/ChildrenProfile/Bloc/children_profile_bloc/children_profile_bloc.dart';
import 'package:junior/Feature/ChildrenProfile/Presentation/Widgets/small_product_widget.dart';
import 'package:junior/Feature/ChildrenProfile/Presentation/Widgets/view_orderDetails.dart';
import 'package:sizer/sizer.dart';

class ChildFoodWidget extends StatelessWidget {
  final String name;
  final int totalPrice;
  final String avatarImage;
  int index;
  final int orderId;
  ChildrenProfileBloc childrenProfileBloc;
  ChildFoodWidget(
      {required this.avatarImage,
      required this.index,
      required this.orderId,
      required this.name,
      required this.totalPrice,
      required this.childrenProfileBloc,
      Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Row(
          children: [
            Expanded(
              child: Container(
                margin: EdgeInsets.only(top: 1.h, right: 1.w, left: 1.w),
                padding: EdgeInsets.symmetric(horizontal: 5.w, vertical: 4.h),
                child: MaterialButton(
                  color: AppColors.fourthColor,
                  shape: RoundedRectangleBorder(
                      side: const BorderSide(
                        width: 3,
                        color: AppColors.primaryColor,
                      ),
                      borderRadius: BorderRadius.circular(5.w)),
                  onPressed: () {
                    viewOrder(context, orderId);
                  },
                  onLongPress: () {
                    childrenProfileBloc
                        .add(RemoveOrder(orderId: orderId, index: index));
                  },
                  child: Column(children: [
                    SizedBox(
                      height: 6.h,
                    ),
                    Row(
                      children: [
                        Text(
                          "Name".tr(context),
                          style: TextStyle(
                              color: AppColors.primaryColor,
                              fontSize: 16.sp,
                              fontWeight: FontWeight.bold),
                        ),
                        SizedBox(
                          width: 10.w,
                        ),
                        Expanded(
                          flex: 1,
                          child: Text(
                            name,
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: AppColors.seconedaryColor,
                              fontSize: 16.sp,
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 1.h,
                    ),
                    // Row(
                    //   children: [
                    //     Expanded(
                    //       child: Text(
                    //         "Illness",
                    //         style: TextStyle(
                    //             color: AppColors.primaryColor,
                    //             fontSize: 16.sp,
                    //             fontWeight: FontWeight.bold),
                    //       ),
                    //     ),
                    //     Expanded(
                    //       flex: 2,
                    //       child: Text(
                    //         illness,
                    //         style: TextStyle(
                    //           color: AppColors.yellowColor,
                    //           fontSize: 16.sp,
                    //         ),
                    //       ),
                    //     ),
                    //   ],
                    // ),
                    // SizedBox(
                    //   height: 1.h,
                    // ),
                    Row(
                      children: [
                        Text(
                          "Total Price".tr(context),
                          style: TextStyle(
                              color: AppColors.primaryColor,
                              fontSize: 16.sp,
                              fontWeight: FontWeight.bold),
                        ),
                        Expanded(
                          flex: 1,
                          child: Text(
                            totalPrice.toString(),
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: AppColors.seconedaryColor,
                              fontSize: 16.sp,
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 2.h,
                    ),
                  ]),
                ),
              ),
            ),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            CircleAvatar(
              radius: 9.w,
              backgroundImage: AssetImage(avatarImage),
            ),
          ],
        ),
      ],
    );

    /*Container(
      padding: EdgeInsets.symmetric(horizontal: 2.w, vertical: 2.h),
      margin: EdgeInsets.symmetric(horizontal: 2.w, vertical: 1.h),
      decoration: BoxDecoration(
          color: AppColors.primaryColor.withOpacity(0.5),
          borderRadius: BorderRadius.circular(5.w),
          border: Border.all(width: 2, color: AppColors.primaryColor)),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              CircleAvatar(
                radius: 8.w,
                backgroundImage: AssetImage(avatarImage),
              ),
            ],
          ),
          SizedBox(
            height: 2.h,
          ),
          Row(
            children: [
              Expanded(
                child: Text(
                  "Name".tr(context),
                  style: TextStyle(
                      color: AppColors.primaryColor,
                      fontSize: 16.sp,
                      fontWeight: FontWeight.bold),
                ),
              ),
              Expanded(
                flex: 2,
                child: Text(
                  name,
                  style: TextStyle(
                    color: AppColors.seconedaryColor,
                    fontSize: 16.sp,
                  ),
                ),
              ),
            ],
          ),
          SizedBox(
            height: 1.h,
          ),
          Row(
            children: [
              Expanded(
                child: Text(
                  "Total Price".tr(context),
                  style: TextStyle(
                      color: AppColors.primaryColor,
                      fontSize: 16.sp,
                      fontWeight: FontWeight.bold),
                ),
              ),
              Expanded(
                flex: 2,
                child: Text(
                  totalPrice.toString(),
                  style: TextStyle(
                    color: AppColors.seconedaryColor,
                    fontSize: 16.sp,
                  ),
                ),
              ),
            ],
          ),
          SizedBox(
            height: 1.h,
          ),
          // Row(
          //   children: [
          //     Expanded(
          //       child: Padding(
          //         padding: EdgeInsets.symmetric(horizontal: 2.w),
          //         child: Column(
          //           children: [
          //             TitleWidget(title: "Foods".tr(context)),
          //             SmallProductWidget(
          //               img: AppAssets.foodIcon,
          //               title: "burgar",
          //               function: () {},
          //             ),
          //           ],
          //         ),
          //       ),
          //     ),
          //     Expanded(
          //       child: Padding(
          //         padding: EdgeInsets.symmetric(horizontal: 2.w),
          //         child: Column(
          //           children: [
          //             TitleWidget(title: "Drink".tr(context)),
          //             SmallProductWidget(
          //               img: AppAssets.foodIcon,
          //               title: "lemon",
          //               function: () {},
          //             ),
          //           ],
          //         ),
          //       ),
          //     )
          //   ],
          // ),
          SizedBox(
            height: 2.h,
          ),
        ],
      ),
    );*/
  }
}
