import 'package:flutter/material.dart';
import 'package:junior/App/app_localizations.dart';
import 'package:junior/Core/Constants/app_assets.dart';
import 'package:junior/Core/Constants/app_colors.dart';
import 'package:sizer/sizer.dart';

class ChildCardWidget extends StatelessWidget {
  String avatarImage;
  String name;
  String state;
  String illness;
  final onPressed;
  ChildCardWidget(
      {required this.state,
      required this.onPressed,
      required this.illness,
      required this.name,
      required this.avatarImage,
      Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Row(
          children: [
            Expanded(
              child: Container(
                margin: EdgeInsets.only(top: 1.h, right: 1.w, left: 1.w),
                padding: EdgeInsets.symmetric(horizontal: 5.w, vertical: 4.h),
                child: MaterialButton(
                 color: AppColors.fourthColor,
                  shape: RoundedRectangleBorder(
                      side: const BorderSide(
                        width: 3,
                        color: AppColors.primaryColor,
                      ),
                      borderRadius: BorderRadius.circular(5.w)),
                  onPressed: onPressed,
                  child: Column(children: [
                    SizedBox(
                      height: 6.h,
                    ),
                    Row(
                      children: [
                        Expanded(
                          child: Text(
                            "Name".tr(context),
                            style: TextStyle(
                                color: AppColors.primaryColor,
                                fontSize: 16.sp,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                        Expanded(
                          flex: 2,
                          child: Text(
                            name,
                            style: TextStyle(
                              color: AppColors.seconedaryColor,
                              fontSize: 16.sp,
                 
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 1.h,
                    ),
                    // Row(
                    //   children: [
                    //     Expanded(
                    //       child: Text(
                    //         "Illness",
                    //         style: TextStyle(
                    //             color: AppColors.primaryColor,
                    //             fontSize: 16.sp,
                    //             fontWeight: FontWeight.bold),
                    //       ),
                    //     ),
                    //     Expanded(
                    //       flex: 2,
                    //       child: Text(
                    //         illness,
                    //         style: TextStyle(
                    //           color: AppColors.yellowColor,
                    //           fontSize: 16.sp,
                    //         ),
                    //       ),
                    //     ),
                    //   ],
                    // ),
                    // SizedBox(
                    //   height: 1.h,
                    // ),
                    Row(
                      children: [
                        Expanded(
                          child: Text(
                            "State".tr(context),
                            style: TextStyle(
                                color: AppColors.primaryColor,
                                fontSize: 16.sp,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                        Expanded(
                          flex: 2,
                          child: Text(
                           ( state == "H" || state =="h")
                                ? "In Home"
                                : state == "B"
                                    ? "At Bus"
                                    : "In School",
                            style: TextStyle(
                              color: AppColors.seconedaryColor,
                              fontSize: 16.sp,
                              
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 2.h,
                    ),
                  ]),
                ),
              ),
            ),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            CircleAvatar(
              radius: 9.w,
              backgroundImage: AssetImage(avatarImage),
            ),
          ],
        ),
      ],
    );
  }
}
