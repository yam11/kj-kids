import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:junior/Core/Api/ExceptionsHandle.dart';
import 'package:junior/Core/Api/Network.dart';
import 'package:junior/Core/Api/Urls.dart';
import 'package:junior/Feature/ChildrenProfile/Models/class_room_model.dart';
import 'package:meta/meta.dart';

part 'child_home_work_event.dart';
part 'child_home_work_state.dart';

class ChildHomeWorkBloc extends Bloc<ChildHomeWorkEvent, ChildHomeWorkState> {
  ClassRoomModel? classRoomModel;
  List<HomeWork> homeWork = [];
  ChildHomeWorkBloc() : super(ChildHomeWorkInitial()) {
    on<ChildHomeWorkEvent>((event, emit) async {
      if (event is ChildHomeWork) {
        emit(LoadingChildHomeWork());
        try {
          final response = await Network.getData(url: "${Urls.classRoom} 10");
          classRoomModel = ClassRoomModel.fromJson(response.data);
          homeWork.addAll(classRoomModel!.data!.homeWork!);
          emit(SuccesToGetChildHomeWork());
        } catch (error) {
          if (error is DioError) {
            emit(ErrorToGetChildHomeWork(
                message: exceptionsHandle(error: error)));
          } else {
            emit(ErrorToGetChildHomeWork(message: "error"));
          }
        }
      }
         if (event is SendHomeWork) {
        emit(LoadingSendHomeWork());
        await Future.delayed(const Duration(seconds: 1));
    
        
          homeWork.removeAt(event.index);
          emit(SuccessSendHomeWork());
        
      }
    });
  }
}
