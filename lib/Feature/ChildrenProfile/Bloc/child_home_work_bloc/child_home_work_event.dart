part of 'child_home_work_bloc.dart';

@immutable
abstract class ChildHomeWorkEvent {}

class ChildHomeWork extends ChildHomeWorkEvent {}
class SendHomeWork extends ChildHomeWorkEvent {
  int index;
  SendHomeWork({required this.index});
}
