part of 'child_home_work_bloc.dart';

@immutable
abstract class ChildHomeWorkState {}

class ChildHomeWorkInitial extends ChildHomeWorkState {}

class SuccesToGetChildHomeWork extends ChildHomeWorkState {}

class ErrorToGetChildHomeWork extends ChildHomeWorkState {
  final String message;
  ErrorToGetChildHomeWork({required this.message});
}

class LoadingChildHomeWork extends ChildHomeWorkState {}
class LoadingSendHomeWork extends ChildHomeWorkState {}
class SuccessSendHomeWork extends ChildHomeWorkState {}
