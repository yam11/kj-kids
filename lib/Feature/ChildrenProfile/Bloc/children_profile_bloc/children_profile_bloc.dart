import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:junior/Core/Api/ExceptionsHandle.dart';
import 'package:junior/Core/Api/Network.dart';
import 'package:junior/Core/Api/Urls.dart';
import 'package:junior/Feature/ChildrenProfile/Models/childern_details_model.dart';
import 'package:junior/Feature/ChildrenProfile/Models/class_room_model.dart';
import 'package:meta/meta.dart';

part 'children_profile_event.dart';
part 'children_profile_state.dart';

class ChildrenProfileBloc
    extends Bloc<ChildrenProfileEvent, ChildrenProfileState> {
  ClassRoomModel? classRoomModel;
  ChildernDetailsModel? childernDetailsModel;
  List<Orders>order=[];
  ChildrenProfileBloc() : super(ChildrenProfileInitial()) {
    on<ChildrenProfileEvent>((event, emit) async {
      if (event is ChildrenTeacher) {
        emit(LoadingToGetChildrenTeacher());
        try {
          final response = await Network.getData(
              url: "${Urls.classRoom} ${event.classRoomId}");
          classRoomModel = ClassRoomModel.fromJson(response.data);
          emit(SuccesToGetChildrenTeacher());
        } catch (error) {
          if (error is DioError) {
            emit(ErrorToGetChildrenTeacher(
                message: exceptionsHandle(error: error)));
          } else {
            emit(ErrorToGetChildrenTeacher(message: "error"));
          }
        }
      }
      if (event is ChildrenDetails) {
        emit(LoadingToGetChildrenDetails());
        try {
          final response = await Network.getData(
              url: "${Urls.childDetails} ${event.childId}");
          childernDetailsModel = ChildernDetailsModel.fromJson(response.data);
          order.addAll(childernDetailsModel!.data!.orders!);
          emit(SuccesToGetChildrenDetails());
        } catch (error) {
          if (error is DioError) {
            emit(ErrorToGetChildrenDetails(
                message: exceptionsHandle(error: error)));
          } else {
            emit(ErrorToGetChildrenDetails(message: "error"));
          }
        }
      }
      if (event is RemoveOrder) {
        emit(LoadingToDeleteOrder());
        try {
          await Network.deleteData(url: "${Urls.viewOrder} ${event.orderId}");
        order.removeAt(event.index);
          emit(SuccesToDeleteOrder());
        } catch (error) {
          if (error is DioError) {
            emit(ErrorToGetChildrenDetails(
                message: exceptionsHandle(error: error)));
          } else {
            emit(ErrorToGetChildrenDetails(message: "error"));
          }
        }
      }
    });
  }
}
