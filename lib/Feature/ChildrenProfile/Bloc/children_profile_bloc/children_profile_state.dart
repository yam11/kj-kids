part of 'children_profile_bloc.dart';

@immutable
abstract class ChildrenProfileState {}

class ChildrenProfileInitial extends ChildrenProfileState {}

class SuccesToGetChildrenTeacher extends ChildrenProfileState {}

class ErrorToGetChildrenTeacher extends ChildrenProfileState {
  final String message;
  ErrorToGetChildrenTeacher({required this.message});
}

class LoadingToGetChildrenTeacher extends ChildrenProfileState {}

class SuccesToGetChildrenDetails extends ChildrenProfileState {}

class ErrorToGetChildrenDetails extends ChildrenProfileState {
  final String message;
  ErrorToGetChildrenDetails({required this.message});
}

class LoadingToGetChildrenDetails extends ChildrenProfileState {}
class LoadingToDeleteOrder extends ChildrenProfileState {}
class SuccesToDeleteOrder extends ChildrenProfileState {}
