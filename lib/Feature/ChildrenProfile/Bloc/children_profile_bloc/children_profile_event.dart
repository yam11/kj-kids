part of 'children_profile_bloc.dart';

@immutable
abstract class ChildrenProfileEvent {}

class ChildrenTeacher extends ChildrenProfileEvent {
  final int classRoomId;
  ChildrenTeacher({required this.classRoomId});
}

class ChildrenDetails extends ChildrenProfileEvent {
  final int childId;
  ChildrenDetails({required this.childId});
}

class RemoveOrder extends ChildrenProfileEvent {
  final int orderId;
  int index;
  RemoveOrder({required this.orderId, required this.index});
}
