part of 'view_order_bloc.dart';

@immutable
abstract class ViewOrderEvent {}
class ViewOrder extends ViewOrderEvent {
  int orderId;
  ViewOrder({required this.orderId});
}
