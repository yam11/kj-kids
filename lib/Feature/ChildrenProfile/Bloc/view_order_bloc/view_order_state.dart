part of 'view_order_bloc.dart';

@immutable
abstract class ViewOrderState {}

class ViewOrderInitial extends ViewOrderState {}

class SuccrsToViewOrder extends ViewOrderState {}

class ErrorToViewOrder extends ViewOrderState {
  String message;
  ErrorToViewOrder({required this.message});
}

class LoadingToViewOrder extends ViewOrderState {}
