import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:junior/Core/Api/ExceptionsHandle.dart';
import 'package:junior/Core/Api/Network.dart';
import 'package:junior/Core/Api/Urls.dart';
import 'package:junior/Feature/ChildrenProfile/Models/view_order_model.dart';
import 'package:meta/meta.dart';

part 'view_order_event.dart';
part 'view_order_state.dart';

class ViewOrderBloc extends Bloc<ViewOrderEvent, ViewOrderState> {
  ViewOrderModel? viewOrderModel;
  ViewOrderBloc() : super(ViewOrderInitial()) {
    on<ViewOrderEvent>((event, emit) async {
      if (event is ViewOrder) {
        emit(LoadingToViewOrder());
        try {
          final response =
              await Network.getData(url: "${Urls.viewOrder} ${event.orderId}");
          viewOrderModel = ViewOrderModel.fromJson(response.data);

          emit(SuccrsToViewOrder());
        } catch (error) {
          if (error is DioError) {
            emit(ErrorToViewOrder(message: exceptionsHandle(error: error)));
          } else {
            emit(ErrorToViewOrder(message: "error"));
          }
        }
      }
    });
  }
}
