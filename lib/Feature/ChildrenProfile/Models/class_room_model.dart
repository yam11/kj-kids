class ClassRoomModel {
  Data? data;

  ClassRoomModel({this.data});

  ClassRoomModel.fromJson(Map<String, dynamic> json) {
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data!.toJson();
    }
    return data;
  }
}

class Data {
  int? id;
  int? teacherId;
  String? level;
  Teacher? teacher;
  List<Children>? children;
  List<HomeWork>? homeWork;

  Data({this.id, this.teacherId, this.level, this.teacher, this.children});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    teacherId = json['teacherId'];
    level = json['level'];
    teacher =
        json['teacher'] != null ? new Teacher.fromJson(json['teacher']) : null;
    if (json['children'] != null) {
      children = <Children>[];
      json['children'].forEach((v) {
        children!.add(new Children.fromJson(v));
      });
    }
     if (json['homeworks'] != null) {
      homeWork = <HomeWork>[];
      json['homeworks'].forEach((v) {
        homeWork!.add(new HomeWork.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['teacherId'] = this.teacherId;
    data['level'] = this.level;
    if (this.teacher != null) {
      data['teacher'] = this.teacher!.toJson();
    }
    if (this.children != null) {
      data['children'] = this.children!.map((v) => v.toJson()).toList();
    }
       if (this.homeWork != null) {
      data['homeworks'] = this.homeWork!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Teacher {
  int? id;
  String? firstName;
  String? lastName;
  String? birthDate;
  String? address;
  String? phoneNumber;

  Teacher(
      {this.id,
      this.firstName,
      this.lastName,
      this.birthDate,
      this.address,
      this.phoneNumber});

  Teacher.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    firstName = json['firstName'];
    lastName = json['lastName'];
    birthDate = json['birthDate'];
    address = json['address'];
    phoneNumber = json['phoneNumber'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['firstName'] = this.firstName;
    data['lastName'] = this.lastName;
    data['birthDate'] = this.birthDate;
    data['address'] = this.address;
    data['phoneNumber'] = this.phoneNumber;
    return data;
  }
}

class Children {
  int? id;
  String? firstName;
  int? guaridanId;
  int? classroomId;
  String? status;
  String? birthDate;

  Children(
      {this.id,
      this.firstName,
      this.guaridanId,
      this.classroomId,
      this.status,
      this.birthDate});

  Children.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    firstName = json['firstName'];
    guaridanId = json['guaridanId'];
    classroomId = json['classroomId'];
    status = json['status'];
    birthDate = json['birthDate'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['firstName'] = this.firstName;
    data['guaridanId'] = this.guaridanId;
    data['classroomId'] = this.classroomId;
    data['status'] = this.status;
    data['birthDate'] = this.birthDate;
    return data;
  }
}

class HomeWork {
  int? classroomId;
  String? name;
  String? description;

  String? expiration;
  String? attachment;
  String? createdAt;

  HomeWork({
    this.classroomId,
    this.name,
    this.description,
    this.expiration,
    this.attachment,
    this.createdAt,
  });

  HomeWork.fromJson(Map<String, dynamic> json) {
    classroomId = json['classroomId'];
    name = json['name'];
    description = json['description'];
    expiration = json['expiration'];
    attachment = json['attachment'];
    createdAt = json['createdAt'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['classroomId'] = this.classroomId;
    data['name'] = this.name;
    data['description'] = this.description;
    data['expiration'] = this.expiration;
    data['attachment'] = this.attachment;
    data['createdAt'] = this.createdAt;
    return data;
  }
}
