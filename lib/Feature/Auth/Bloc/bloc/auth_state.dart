part of 'auth_bloc.dart';

@immutable
abstract class AuthState {}

class AuthInitial extends AuthState {}

class SuccesToLogIn extends AuthState {}

class LoadingToLogIn extends AuthState {}

class ErrorToLogIn extends AuthState {
  final String message;
  ErrorToLogIn({required this.message});
}
