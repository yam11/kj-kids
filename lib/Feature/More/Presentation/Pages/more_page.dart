import 'package:animate_do/animate_do.dart';
import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:junior/App/app_localizations.dart';
import 'package:junior/Core/Constants/app_assets.dart';
import 'package:junior/Core/Constants/app_colors.dart';
import 'package:junior/Core/Util/SharedPreferences/SharedPreferencesHelper.dart';
import 'package:junior/Feature/ChildrenProfile/Presentation/Pages/children_food_page.dart';
import 'package:junior/Feature/ChildrenProfile/Presentation/Pages/children_profile_page.dart';
import 'package:junior/Feature/ChildrenProfile/Presentation/Pages/children_teacher_page.dart';
import 'package:junior/Feature/More/Presentation/Widgets/more_select_widget.dart';
import 'package:junior/Feature/MyProfile/Presentation/Pages/my_profile_page.dart';
import 'package:junior/Feature/Setting/Presentation/Pages/setting_page.dart';
import 'package:junior/Feature/Splash/Presentation/Pages/splash_page.dart';
import 'package:page_transition/page_transition.dart';

import 'package:sizer/sizer.dart';

class MorePage extends StatelessWidget {
  const MorePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: SingleChildScrollView(
        physics: const BouncingScrollPhysics(),
        child: Column(
          children: [
            FadeInLeft(
              duration: const Duration(milliseconds: 600),
              child: MoreSelectWidget(
                title: "My profile".tr(context),
                image: AppAssets.myProfileIcon,
                onPressed: () {
                  Navigator.push(
                      context,
                      PageTransition(
                          child: MyProfilePage(),
                          type: PageTransitionType.rightToLeft,
                          duration: const Duration(milliseconds: 300)));
                },
              ),
            ),
            FadeInLeft(
              duration: const Duration(milliseconds: 800),
              child: MoreSelectWidget(
                title: "My children Profile".tr(context),
                image: AppAssets.profileIcon,
                onPressed: () {
                  Navigator.push(
                      context,
                      PageTransition(
                          child: ChildrenProfile(),
                          type: PageTransitionType.rightToLeft,
                          duration: const Duration(milliseconds: 300)));
                },
              ),
            ),
            // FadeInLeft(
            //   duration: const Duration(milliseconds: 1000),
            //   child: MoreSelectWidget(
            //     title: "Children teacher".tr(context),
            //     image: AppAssets.teacherIcon,
            //     onPressed: () {
              
            //     },
            //   ),
            // ),
            // FadeInLeft(
            //   duration: const Duration(milliseconds: 1200),
            //   child: MoreSelectWidget(
            //     title: "My children food".tr(context),
            //     image: AppAssets.foodIcon,
            //     onPressed: () {
            //       Navigator.push(
            //           context,
            //           PageTransition(
            //               child: MyChildrenFoodPage(),
            //               type: PageTransitionType.rightToLeft,
            //               duration: const Duration(milliseconds: 300)));
            //     },
            //   ),
            // ),
            FadeInLeft(
              duration: const Duration(milliseconds: 1400),
              child: MoreSelectWidget(
                title: "Settings".tr(context),
                image: AppAssets.settingsIcons,
                onPressed: () {
                  Navigator.push(
                      context,
                      PageTransition(
                          child: SettingPage(),
                          type: PageTransitionType.rightToLeft,
                          duration: const Duration(milliseconds: 300)));
                },
              ),
            ),
            FadeInLeft(
              duration: const Duration(milliseconds: 1600),
              child: MoreSelectWidget(
                title: "Log out".tr(context),
                image: AppAssets.logoutIcon,
                onPressed: () {
                  AwesomeDialog(
                    title: "Are you sure".tr(context),
                    titleTextStyle: TextStyle(
                        color: AppColors.primaryColor,
                        fontSize: 16.sp,
                        fontWeight: FontWeight.bold),
                    dialogBackgroundColor: Colors.white,
                    btnOkColor: AppColors.primaryColor,
                    btnCancelColor: AppColors.seconedaryColor,
                    btnOkText: "Yes".tr(context),
                    btnCancelText: "No".tr(context),
                    btnOkOnPress: ()  {
                       AppSharedPreferences.removeToken();
                           AppSharedPreferences.removeId();
                      Navigator.of(context).pushAndRemoveUntil(
                          PageTransition(
                              child: SplashPage(),
                              type: PageTransitionType.rightToLeft,
                              duration: const Duration(milliseconds: 300)),
                          ModalRoute.withName("/"));
                    },
                    btnCancelOnPress: () {},
                    dismissOnTouchOutside: false,
                    context: context,
                    dialogType: DialogType.noHeader,
                    animType: AnimType.rightSlide,
                  ).show();
                },
              ),
            ),
            SizedBox(
              height: 4.h,
            )
          ],
        ),
      ),
    );
  }
}
