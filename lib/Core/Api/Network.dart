import 'dart:io';

import 'package:dio/adapter.dart';
import 'package:dio/dio.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';

import '../Util/SharedPreferences/SharedPreferencesHelper.dart';

class Network {
  static late Dio dio;

  static init() {
    dio = Dio(BaseOptions(headers: {
      // 'lang': "en",

      // 'Authorization': "Bearer ${AppSharedPreferences.getToken}",

      'Content-Type': 'application/json',
      "Accept": 'application/json',
      "Accept-Charset": "application/json"
    }));

    dio.interceptors.add(PrettyDioLogger(
        requestHeader: false,
        requestBody: true,
        responseBody: true,
        responseHeader: false,
        error: true,
        request: true,
        compact: true,
        maxWidth: 1000));

    (dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate =
        (HttpClient client) {
      client.badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;

      return client;
    };
  }

  static Future<Response> getData({
    required String url,
  }) async {
    dio.options.headers = {
      // 'lang': AppSharedPreferences.getLang,
      // 'Authorization': "Bearer ${AppSharedPreferences.getToken}",
      'Content-Type': 'application/json'
    };
    final response = await dio.get(
      url,
    );
    return response;
  }

  static Future<Response> postData({
    required String url,
    String? token,
    Map<String, dynamic>? query,
    var data,
  }) async {
    dio.options.headers = {
      // 'lang': AppSharedPreferences.getLang,
      // 'Authorization': "Bearer ${AppSharedPreferences.getToken}",
      'Content-Type': 'application/json'
    };
    return await dio.post(
      url,
      data: data,
    );
  }

  static Future<Response> putData({
    required String url,
    Map<String, dynamic>? query,
    required Map<String, dynamic> data,
  }) async {
    dio.options.headers = {
      // 'lang': AppSharedPreferences.getLang,
      // 'Authorization': "Bearer ${AppSharedPreferences.getToken}",
      'Content-Type': 'application/json'
    };
    return await dio.put(url, data: data);
  }

  static Future<Response> deleteData({
    required String url,
    Map<String, dynamic>? query,
    Map<String, dynamic>? data,
  }) async {
    return await dio.delete(url, data: data);
  }
}
