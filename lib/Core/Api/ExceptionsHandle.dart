

import 'package:dio/dio.dart';

String exceptionsHandle
    ({
  required DioError error
})
{
  final String message;
  switch (error.type)
  {

    case  DioErrorType.connectTimeout :
      message = 'السيرفر لا يستجيب';
      break;

    case DioErrorType.sendTimeout:
      message = 'send Time out';
      break;
    case DioErrorType.receiveTimeout:
      message = 'السيرفر لا يستجيب';
      break;
    case DioErrorType.response:
         message = error.response!.data['message'];
      break;
    case DioErrorType.cancel:
      message = 'request is cancelled';
      break;
    case DioErrorType.other:
      error.message.contains('SocketException') ?
      message = 'تحقق من وجود الانترنت '
          : message = error.message ;
      break;


  }

  return message ;
}