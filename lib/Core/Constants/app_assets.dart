class AppAssets {
  /*images*/
  static const String homeBackGraound = "assets/images/Edges.png";
  static const String yallow = "assets/images/yallow.png";
  static const String purple = "assets/images/purple.png";
  static const String blue = "assets/images/blue.png";
  static const String motherImage = "assets/images/mother.png";
  static const String studentImage = "assets/images/student.png";
  static const String kidImage = "assets/images/kid.png";
  static const String aboutUsImage = "assets/images/aboutus.png";


  /*icons*/
  static const String foodIcon = "assets/icons/dinner.png";
  static const String profileIcon = "assets/icons/user.png";
  static const String settingsIcons = "assets/icons/settings.png";
  static const String logoutIcon = "assets/icons/exit.png";
  static const String myProfileIcon = "assets/icons/myprofile.png";
  static const String teacherIcon = "assets/icons/teacher.png";
  static const String womanIcon = "assets/icons/woman.png";
  static const String reportIcon = "assets/icons/report.png";
  static const String resultsIcon = "assets/icons/results.png";
  static const String homeworkIcon = "assets/icons/homework.png";
}
