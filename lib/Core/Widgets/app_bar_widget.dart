import 'package:flutter/material.dart';
import 'package:junior/Core/Constants/app_colors.dart';
import 'package:sizer/sizer.dart';

class AppBarWidget extends StatelessWidget {
  const AppBarWidget({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppBar(
        centerTitle: true,
        elevation: 0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(10.w),
              bottomRight: Radius.circular(10.w)),
        ),
        backgroundColor: AppColors.primaryColor,
        leading: IconButton(
          onPressed: (() {
            Navigator.pop(context);
          }),
          icon: IconButton(
              icon: Icon(
                Icons.arrow_back_ios,
                color: AppColors.seconedaryColor,
                size: 5.w,
              ),
              onPressed: () {
                Navigator.of(context).pop();
              }),
        ),
        title: Text(
          "Kiddies",
          textAlign: TextAlign.center,
          style: TextStyle(
              fontSize: 30.sp,
              fontWeight: FontWeight.bold,
              color: AppColors.seconedaryColor),
        ));
  }
}
