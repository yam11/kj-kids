import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:junior/Core/Constants/app_colors.dart';
import 'package:sizer/sizer.dart';

class PickImage extends StatefulWidget {
  void Function(String imagePath) onImagePicked;
  PickImage({required this.onImagePicked, Key? key}) : super(key: key);

  @override
  State<PickImage> createState() => _PickImageState();
}

class _PickImageState extends State<PickImage> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 30.h,
      decoration: const BoxDecoration(
        color: AppColors.whiteColor,
      ),
      child: Column(
        children: [
          SizedBox(
            height: 2.h,
          ),
          Text(
            "Chose Image",
            style: TextStyle(
                color: AppColors.seconedaryColor,
                fontSize: 18.sp,
                fontWeight: FontWeight.bold),
          ),
          Expanded(
            child: Row(
              children: [
                Expanded(
                  child: MaterialButton(
                    onPressed: () async {
                      ImagePicker picker = ImagePicker();
                      final file =
                          await picker.pickImage(source: ImageSource.gallery);
                      if (file != null) widget.onImagePicked.call(file.path);
                      Navigator.of(context).pop();
                    },
                    child: Icon(
                      Icons.photo,
                      size: 25.w,
                      color: AppColors.primaryColor,
                    ),
                  ),
                ),
                Expanded(
                  child: MaterialButton(
                    onPressed: () async {
                      ImagePicker picker = ImagePicker();
                      final file =
                          await picker.pickImage(source: ImageSource.camera);
                      if (file != null) widget.onImagePicked.call(file.path);
                      Navigator.of(context).pop();
                    },
                    child: Icon(
                      Icons.camera_alt_outlined,
                      size: 25.w,
                      color: AppColors.primaryColor,
                    ),
                  ),
                ),
              ],
            ),
          ),
          MaterialButton(
            color: AppColors.primaryColor,
            padding: EdgeInsets.symmetric(horizontal: 2.w, vertical: 1.h),
            onPressed: () {
              Navigator.of(context).pop();
            },
            child: Text(
              "Cancle",
              style: TextStyle(
                  color: AppColors.seconedaryColor,
                  fontSize: 14.sp,
                  fontWeight: FontWeight.bold),
            ),
          ),
          SizedBox(
            height: 2.h,
          ),
        ],
      ),
    );
  }
}
