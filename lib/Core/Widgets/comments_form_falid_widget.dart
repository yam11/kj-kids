import 'package:flutter/material.dart';
import 'package:junior/Core/Constants/app_colors.dart';
import 'package:sizer/sizer.dart';

class CommentsFormFalidWidget extends StatelessWidget {
  TextInputType textInputType;

  final TextEditingController controller;
  int maxLines;

  CommentsFormFalidWidget(
      {required this.controller,
      required this.maxLines,
      required this.textInputType,
      Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 5.w),
      decoration: BoxDecoration(boxShadow: [
        BoxShadow(
          color: const Color.fromARGB(255, 135, 152, 167).withOpacity(0.5),
          spreadRadius: 1,
          blurRadius: 3,
          offset: const Offset(0, 3), // changes position of shadow
        ),
      ]),
      child: TextFormField(
        keyboardType: textInputType,
        controller: controller,
        maxLines: maxLines,
        cursorColor: AppColors.primaryColor,
        cursorHeight: 3.h,
        decoration: InputDecoration(
          filled: true,
          fillColor: Colors.white,
          enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(5.w),
                  topRight: Radius.circular(5.w)),
              borderSide: const BorderSide(color: AppColors.whiteColor)),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(5.w),
                topRight: Radius.circular(5.w)),
            borderSide: const BorderSide(color: AppColors.primaryColor),
          ),
          border: OutlineInputBorder(
              borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(5.w),
                  topRight: Radius.circular(5.w)),
              borderSide: const BorderSide(color: AppColors.primaryColor)),
        ),
        style: TextStyle(
          fontSize: 14.sp,
          color: AppColors.primaryColor,
        ),
      ),
    );
  }
}
