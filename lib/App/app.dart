import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:junior/App/Bloc/app_language_cubit/app_language_cubit.dart';
import 'package:junior/App/app_localizations.dart';
import 'package:junior/Core/Util/SharedPreferences/SharedPreferencesHelper.dart';

import 'package:junior/Feature/Auth/Presentation/Pages/log_in_page.dart';
import 'package:junior/Feature/Main/Presentation/Pages/main_page.dart';

import 'package:junior/Feature/Splash/Presentation/Pages/splash_page.dart';
import 'package:sizer/sizer.dart';

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return Sizer(
      builder: (context, orientation, deviceType) {
        return MultiBlocProvider(
            providers: [
              BlocProvider(
                create: (_) => AppLanguageCubit()..getLanguage(),
              ),
            ],
            child: BlocConsumer<AppLanguageCubit, ChangeLanguage>(
              listener: (context, state) {},
              builder: (context, state) {
                return MaterialApp(
                  locale: Locale(AppSharedPreferences.getArLang),
                  supportedLocales: const [Locale('en', 'US'), Locale('ar')],
                  localizationsDelegates: const [
                    Applocalizations.delegate,
                    GlobalMaterialLocalizations.delegate,
                    GlobalWidgetsLocalizations.delegate,
                    GlobalCupertinoLocalizations.delegate
                  ],
                  localeResolutionCallback: (deviceLocale, supportedLocales) {
                    for (var locale in supportedLocales) {
                      if (deviceLocale != null &&
                          deviceLocale.languageCode == locale.languageCode) {
                        return deviceLocale;
                      }
                    }
                    return supportedLocales.first;
                  },
                  title: 'KJ',
                  debugShowCheckedModeBanner: false,
                  home: SplashPage(),
                );
              },
            ));
      },
    );
  }
}
