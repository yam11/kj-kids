part of 'app_language_cubit.dart';

@immutable


class ChangeLanguage {
  final Locale locale;

  ChangeLanguage({required this.locale});
}
