import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:junior/App/app.dart';
import 'package:junior/Core/Api/Network.dart';
import 'package:junior/Core/Util/SharedPreferences/SharedPreferencesHelper.dart';
import 'package:junior/Feature/Setting/Models/codegen_loader.g.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await AppSharedPreferences.init();
  await Network.init();
  print("ID is ${AppSharedPreferences.getId}");
  print("token is ${AppSharedPreferences.getToken}");
  print("Language is ${AppSharedPreferences.getArLang}");
  runApp(MyApp());
}
